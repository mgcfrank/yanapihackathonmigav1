new Vue({
    el: '#layoutPagina',
    data: {
        id_post:(POST_.split("_-_"))[1],
        postSeleccionado:{},
        postContenido:null,
        txtBuscar:'',
        articuloArray:{},
    },
    methods: {
        getIconCategoria(tipo){
            switch(tipo) {
                case 'Ciencia':
                    return 'fa fa-vial';
                break;
                case 'Historia':
                    return 'fa fa-landmark';
                break;

                case 'Tecnologia':
                    return 'fas fa-microchip';
                break;
                case 'Cine':
                    return 'fas fa-video';
                break;

                case 'Arte':
                    return 'fas fa-palette';
                break;
                case 'Cultura general':
                    return 'fas fa-user-graduate';
                break;

                case 'Terror':
                    return 'fab fa-optin-monster';
                break;
                case 'Deporte':
                    return 'fas fa-futbol';
                break;

                case 'Musica':
                    return 'fas fa-music';
                break;
                case 'Naturaleza':
                    return 'fas fa-dove';
                break;

                case 'Pasatiempo':
                    return 'fa fa-cut';
                break;
            }
        },
        buscaCategoria: function(categoria){
            my_form=document.createElement('FORM');
            my_form.name='myForm';
            my_form.method='POST';
            my_form.action=BASE_URL+"UPost/getPostBusca";

            my_tb=document.createElement('INPUT');
            my_tb.type='TEXT';
            my_tb.name='categoria';
            my_tb.value=categoria;
            my_form.appendChild(my_tb);

            my_tb=document.createElement('INPUT');
            my_tb.type='HIDDEN';
            my_tb.name='busca';
            my_tb.value=this.txtBuscar;
            my_form.appendChild(my_tb);
            document.body.appendChild(my_form);
            my_form.submit();
        },
        recuperaArticuloBaner: function() {
            console.log(this.id_post);
            this.vueToast('Cargando datos post...', 'success');
            this.$http.post(BASE_URL+"UPost/getPostVista", {
                id_post: this.id_post
            }).then(function(respuesta) {
                this.postSeleccionado = respuesta.body;
                this.postContenido=((this.postSeleccionado).contenido).split("_-_");
                // if (this.postContenido[(this.postContenido).length]==null) {
                //     this.postContenido[(this.postContenido).length]
                // }
                console.log(this.postSeleccionado);
                console.log(this.postContenido);
            }, function() {
                this.vueToast('Error en la conexion', 'error');
                alert('No se han encontrado: Empresas');
            });
        },
        recuperaArticulosSugeridos(){
            this.$http.post(BASE_URL+"UMain/getArticuloBaner").then(function(respuesta) {
                this.articuloArray = respuesta.body;
                console.log(this.articuloArray);
            }, function() {
                this.vueToast('Error en la conexion', 'error');
                alert('No se han encontrado: Empresas');
            });
        },
        //EXTRAS
        fechaLiteral: function(fecha) {
            if (fecha != null || fecha != '') {
                let dias = ["Lun", "Mar", "Mier", "Jue", "Vie", "Sab", "Dom"];
                let meses = ["Ene.", "Feb.", "Mar.", "Abr.", "May.", "Jun.", "Jul.", "Ago.", "Sept.", "Oct.", "Nov.", "Dic."];
                let date = new Date(fecha);
                let fechaNum = date.getUTCDate();
                let mes_name = date.getMonth();
                return (dias[date.getDay()] + ", " + fechaNum + " " + meses[mes_name] + " de " + date.getFullYear());
            } else {
                return '';
            }
        },
        filtroCEspeciales: function(texto){
            texto=texto.normalize('NFD').replace(/[\u0300-\u036f]/g,'');//quita tildes
            texto=texto.replace(/[^a-z0-9\s]/gi, '');//vuelve a carateres alfanumericos
            texto=texto.replace(/\s/g,'-');
            return texto
        },
        vueToast: function(mensaje, tipo) {
            //info success error show
            let toast = this.$toasted.show(mensaje, {
                theme: "toasted-primary",
                position: "bottom-center",
                duration: 2000,
                fullWidth: false,
                type: tipo
            });
        }
    },
    created: function() {
        // this.recuperaArticuloBaner();
        // this.recuperaArticulosSugeridos();
    }
});