new Vue({
    el: '#layoutPagina',
    data: {
        articuloArray: [
            {autor:'',categoria: '', contenido: '', f_alta: '', foto: 'x', idpost: '', titulo: '', visitas: ''},
            {autor:'',categoria: '', contenido: '', f_alta: '', foto: 'x', idpost: '', titulo: '', visitas: ''},
            {autor:'',categoria: '', contenido: '', f_alta: '', foto: 'x', idpost: '', titulo: '', visitas: ''},
        ],
        cienciaArray: [
            {autor:'',categoria: '', contenido: '', f_alta: '', foto: '', idpost: '', titulo: '', visitas: ''},
            {autor:'',categoria: '', contenido: '', f_alta: '', foto: '', idpost: '', titulo: '', visitas: ''},
            {autor:'',categoria: '', contenido: '', f_alta: '', foto: '', idpost: '', titulo: '', visitas: ''},
        ],
        cineArray: [
            {autor:'',categoria: '', contenido: '', f_alta: '', foto: '', idpost: '', titulo: '', visitas: ''},
            {autor:'',categoria: '', contenido: '', f_alta: '', foto: '', idpost: '', titulo: '', visitas: ''},
            {autor:'',categoria: '', contenido: '', f_alta: '', foto: '', idpost: '', titulo: '', visitas: ''},
        ],
        terrorArray: [],
        tecnologiaArray: [],
        culturaGeneralArray: [],
        txtEstado:'x',
        txtBuscar:'',
    },
    methods: {
        getIconCategoria(tipo){
            switch(tipo) {
                case 'Ciencia':
                    return 'fa fa-vial';
                break;
                case 'Historia':
                    return 'fa fa-landmark';
                break;

                case 'Tecnologia':
                    return 'fas fa-microchip';
                break;
                case 'Cine':
                    return 'fas fa-video';
                break;

                case 'Arte':
                    return 'fas fa-palette';
                break;
                case 'Cultura general':
                    return 'fas fa-user-graduate';
                break;

                case 'Terror':
                    return 'fab fa-optin-monster';
                break;
                case 'Deporte':
                    return 'fas fa-futbol';
                break;

                case 'Musica':
                    return 'fas fa-music';
                break;
                case 'Naturaleza':
                    return 'fas fa-dove';
                break;

                case 'Pasatiempo':
                    return 'fa fa-cut';
                break;
            }
        },
        buscaCategoria: function(categoria){
            my_form=document.createElement('FORM');
            my_form.name='myForm';
            my_form.method='POST';
            my_form.action=BASE_URL+"UPost/getPostBusca";

            my_tb=document.createElement('INPUT');
            my_tb.type='TEXT';
            my_tb.name='categoria';
            my_tb.value=categoria;
            my_form.appendChild(my_tb);

            my_tb=document.createElement('INPUT');
            my_tb.type='HIDDEN';
            my_tb.name='busca';
            my_tb.value=this.txtBuscar;
            my_form.appendChild(my_tb);
            document.body.appendChild(my_form);
            my_form.submit();
        },
        recuperaArticuloBaner: function() {
            this.vueToast('Cargando datos...', 'success');
            this.txtEstado='Cargando baners...';
            this.$http.post("UMain/getArticuloBaner").then(function(respuesta) {
                this.articuloArray = respuesta.body;
                console.log(this.articuloArray);
                // for (var i = 0; i < (this.anuncioArray).length; i++) {
                //     this.anuncioArray[i].fecha=this.fechaLiteral(this.anuncioArray[i].fecha);
                // }
                // // this.anuncioGuardado=this.anuncioArray[0];
                //ciencia
                this.txtEstado='Cargando articulos de ciencia...';
                this.$http.post("UMain/getArticuloCiencia5", {
                    categoria: 'Ciencia'
                }).then(function(respuesta) {
                    this.cienciaArray = respuesta.body;
                    console.log(this.cienciaArray);
                    this.txtEstado='Cargando articulos de cine...';
                    //cine
                    this.$http.post("UMain/getArticuloCiencia5", {
                        categoria: 'Cine'
                    }).then(function(respuesta) {
                        this.cineArray = respuesta.body;
                        console.log(this.cineArray);
                        
                    }, function() {
                        this.vueToast('Error en la conexion', 'error');
                        alert('No se han encontrado: Empresas');
                    });

                    //terror
                    this.txtEstado='Cargando articulos de terror...';
                        this.$http.post("UMain/getArticuloCiencia5", {
                            categoria: 'Terror'
                        }).then(function(respuesta) {
                            this.terrorArray = respuesta.body;
                            console.log(this.terrorArray);
                            //tecnologia
                            this.txtEstado='Cargando articulos de tecnologia...';
                            this.$http.post("UMain/getArticuloCiencia5", {
                                categoria: 'Tecnologia'
                            }).then(function(respuesta) {
                                this.tecnologiaArray = respuesta.body;
                                console.log(this.tecnologiaArray);
                                //cultura general
                                this.txtEstado='Cargando articulos de cultura general...';
                                this.$http.post("UMain/getArticuloCiencia5", {
                                    categoria: 'Cultura general'
                                }).then(function(respuesta) {
                                    this.culturaGeneralArray = respuesta.body;
                                    console.log(this.culturaGeneralArray);
                                    this.txtEstado='x';
                                }, function() {
                                    this.vueToast('Error en la conexion', 'error');
                                    alert('No se han encontrado: Empresas');
                                });
                            }, function() {
                                this.vueToast('Error en la conexion', 'error');
                                alert('No se han encontrado: Empresas');
                            });

                        }, function() {
                            this.vueToast('Error en la conexion', 'error');
                            alert('No se han encontrado: Empresas');
                        });
                }, function() {
                    this.vueToast('Error en la conexion', 'error');
                    alert('No se han encontrado: Empresas');
                });
            }, function() {
                this.vueToast('Error en la conexion', 'error');
                alert('No se han encontrado: Empresas');
            });
        },
        //EXTRAS
        fechaLiteral: function(fecha) {
            if (fecha != null || fecha != '') {
                let dias = ["Lun", "Mar", "Mier", "Jue", "Vie", "Sab", "Dom"];
                let meses = ["Ene.", "Feb.", "Mar.", "Abr.", "May.", "Jun.", "Jul.", "Ago.", "Sept.", "Oct.", "Nov.", "Dic."];
                let date = new Date(fecha);
                let fechaNum = date.getUTCDate();
                let mes_name = date.getMonth();
                return (dias[date.getDay()] + ", " + fechaNum + " " + meses[mes_name] + " de " + date.getFullYear());
            } else {
                return '';
            }
        },
        filtroCEspeciales: function(texto){
            texto=texto.normalize('NFD').replace(/[\u0300-\u036f]/g,'');//quita tildes
            texto=texto.replace(/[^a-z0-9\s]/gi, '');//vuelve a carateres alfanumericos
            texto=texto.replace(/\s/g,'-');
            return texto
        },
        vueToast: function(mensaje, tipo) {
            //info success error show
            let toast = this.$toasted.show(mensaje, {
                theme: "toasted-primary",
                position: "bottom-center",
                duration: 2000,
                fullWidth: false,
                type: tipo
            });
        }
    },
    created: function() {
        // altoW=parseInt((parseInt(screen.height)*77)/100);
        // console.log(screen.height+" - "+altoW);
        // var obj = document.getElementById('panel-img');
        // obj.style.height = altoW+'px';
        // this.recuperaArticuloBaner();
        // this.constructMap();
    },
    computed: {
        // buscarAnuncio: function(){
        //     return this.anuncioArray.filter((item) => {
        //         return item.titulo.toLowerCase().includes(this.txtBuscarAnuncio.toLowerCase()) ||
        //         item.descripcion.toLowerCase().includes(this.txtBuscarAnuncio.toLowerCase());
        //     });
        // }
    }
});