// VALIDAR SI EL NAVEGADOR SOPORTA SERVICE WORKER
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('public/pwa/sw.js').then(reg => console.log('Registro de SW exitoso', reg)).catch(err => console.warn('Error al tratar de registrar el sw', err))
}