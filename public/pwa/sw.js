//asignar un nombre y versión al cache
const CACHE_NAME = 'v1_cache_programador_fitness';
urlsToCache = ['./', 'https://yanapi.quiboo.org/public/images/icono-quibo-logo-bolivia.png', 'https://use.fontawesome.com/releases/v5.8.2/css/all.css', 'https://yanapi.quiboo.org/public/css/bootstrap.css?n=0.1', 'https://yanapi.quiboo.org/public/js/jquery-1.9.1.min.js?n=0.1', 'https://yanapi.quiboo.org/public/js/bootstrap.min.js?n=0.1', 'https://yanapi.quiboo.org/public/js/bootstrap.bundle.min.js?n=0.1', 'https://yanapi.quiboo.org/public/js/viewer.js?n=0.1', 'https://yanapi.quiboo.org/public/css/viewer.css?n=0.1', 'https://yanapi.quiboo.org/public/js/vue.min.js?n=0.1', 'https://yanapi.quiboo.org/public/js/vue-resource.min.js?n=0.1', 'https://yanapi.quiboo.org/public/js/vue-toasted.min.js?n=0.1', 'https://unpkg.com/leaflet@1.6.0/dist/leaflet.css', 'https://unpkg.com/leaflet@1.6.0/dist/leaflet.js', 'https://yanapi.quiboo.org/public/images/background-quibo-developer.jpg', 'https://yanapi.quiboo.org/public/folder/img_categoria/yanapi-restaurantes.png', 'https://yanapi.quiboo.org/public/folder/img_categoria/yanapi-tiendas.png', 'https://yanapi.quiboo.org/public/folder/img_categoria/yanapi-panaderias.png', 'https://yanapi.quiboo.org/public/folder/img_categoria/yanapi-zanahoria.png', 'https://yanapi.quiboo.org/public/folder/img_categoria/yanapi-lo-que-sea.png', 'https://yanapi.quiboo.org/public/folder/img_categoria/yanapi-naturistas.png', 'https://yanapi.quiboo.org/public/folder/img_categoria/yanapi-carniceria.png', 'https://yanapi.quiboo.org/public/folder/img_categoria/yanapi-farmacias.png', 'https://yanapi.quiboo.org/public/folder/img_categoria/yanapi-ferreterias.png', ];
//durante la fase de instalación, generalmente se almacena en caché los activos estáticos
self.addEventListener('install', e => {
    e.waitUntil(caches.open(CACHE_NAME).then(cache => {
        return cache.addAll(urlsToCache).then(() => self.skipWaiting())
    }).catch(err => console.log('Falló registro de cache', err)))
});
//una vez que se instala el SW, se activa y busca los recursos para hacer que funcione sin conexión
self.addEventListener('activate', e => {
    const cacheWhitelist = [CACHE_NAME]
    e.waitUntil(caches.keys().then(cacheNames => {
            return Promise.all(cacheNames.map(cacheName => {
                //Eliminamos lo que ya no se necesita en cache
                if (cacheWhitelist.indexOf(cacheName) === -1) {
                    return caches.delete(cacheName)
                }
            }))
        })
        // Le indica al SW activar el cache actual
        .then(() => self.clients.claim()))
});
//cuando el navegador recupera una url
self.addEventListener('fetch', e => {
    //Responder ya sea con el objeto en caché o continuar y buscar la url real
    e.respondWith(caches.match(e.request).then(res => {
        if (res) {
            //recuperar del cache
            return res
        }
        //recuperar de la petición a la url
        return fetch(e.request)
    }))
});