-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-07-2020 a las 01:28:00
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hackaton`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `idCategoria` varchar(150) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `foto` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` varchar(150) NOT NULL,
  `nombres` varchar(60) DEFAULT NULL,
  `apellidos` varchar(60) DEFAULT NULL,
  `numero` varchar(20) DEFAULT NULL,
  `ci` int(12) NOT NULL DEFAULT 0,
  `foto` varchar(150) DEFAULT NULL,
  `estado` varchar(20) NOT NULL,
  `f_alta` datetime DEFAULT NULL,
  `f_modificacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle`
--

CREATE TABLE `detalle` (
  `iddetalle` varchar(150) NOT NULL,
  `idProducto` varchar(150) NOT NULL,
  `idPedido` varchar(150) NOT NULL,
  `titulo_producto` varchar(60) NOT NULL DEFAULT 'Sin producto',
  `cantidad` varchar(45) DEFAULT NULL,
  `precio_actual` varchar(45) DEFAULT NULL,
  `f_alta` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `idEmpresa` varchar(150) NOT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `categorias` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  `encargado` varchar(100) DEFAULT NULL,
  `numero` varchar(20) DEFAULT NULL,
  `whatsapp` varchar(20) DEFAULT NULL,
  `messenger` varchar(100) NOT NULL DEFAULT 'No',
  `foto` varchar(100) DEFAULT NULL,
  `precio_envio_metro` double(8,2) DEFAULT NULL,
  `delivery` enum('Si','No') NOT NULL,
  `estado` enum('Activo','Inactivo','Ocupado') DEFAULT NULL,
  `mayorista` enum('Si','No') DEFAULT NULL,
  `minorista` enum('Si','No') DEFAULT NULL,
  `f_alta` datetime NOT NULL,
  `f_modificacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `idPedido` varchar(150) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `precio_envio` double(10,2) DEFAULT NULL,
  `precio_pedido` double(10,2) DEFAULT NULL,
  `estado` varchar(20) DEFAULT NULL COMMENT 'Enviado/Recibido/Entregado/Cancelado',
  `direccion` varchar(80) DEFAULT NULL,
  `latitud` double(18,10) DEFAULT NULL,
  `longitud` double(18,10) DEFAULT NULL,
  `f_alta` datetime DEFAULT NULL,
  `f_modificacion` datetime DEFAULT NULL,
  `idCliente` varchar(150) NOT NULL,
  `idEmpresa` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idProducto` varchar(150) NOT NULL,
  `titulo` varchar(60) DEFAULT NULL,
  `descripcion` varchar(120) DEFAULT NULL,
  `precio` double(10,2) DEFAULT NULL,
  `stock` bigint(20) DEFAULT NULL,
  `foto` varchar(150) DEFAULT NULL,
  `f_alta` datetime DEFAULT NULL,
  `f_modificacion` datetime DEFAULT NULL,
  `idEmpresa` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reportes`
--

CREATE TABLE `reportes` (
  `idReportes` varchar(150) NOT NULL,
  `detalle` varchar(60) DEFAULT NULL,
  `calificacion` double(2,2) NOT NULL DEFAULT 0.99,
  `idCliente` varchar(150) NOT NULL,
  `idEmpresa` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursalgps`
--

CREATE TABLE `sucursalgps` (
  `idSucursal` varchar(150) NOT NULL,
  `idEmpresa` varchar(150) NOT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `latitud` double(18,10) DEFAULT NULL,
  `longitud` double(18,10) DEFAULT NULL,
  `horario_atencion` longtext DEFAULT '[{"dia":1,"hora1":"09:00","hora2":"13:00"},{"dia":2,"hora1":"09:00","hora2":"13:00"},{"dia":3,"hora1":"09:00","hora2":"13:00"},{"dia":4,"hora1":"09:00","hora2":"13:00"},{"dia":5,"hora1":"09:00","hora2":"13:00"},{"dia":6,"hora1":"09:00","hora2":"13:00"},{"dia":7,"hora1":"09:00","hora2":"13:00"}]',
  `horario_pedidos` longtext DEFAULT '[{"dia":1,"hora1":"09:00","hora2":"13:00"},{"dia":2,"hora1":"09:00","hora2":"13:00"},{"dia":3,"hora1":"09:00","hora2":"13:00"},{"dia":4,"hora1":"09:00","hora2":"13:00"},{"dia":5,"hora1":"09:00","hora2":"13:00"},{"dia":6,"hora1":"09:00","hora2":"13:00"},{"dia":7,"hora1":"09:00","hora2":"13:00"}]'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temporada`
--

CREATE TABLE `temporada` (
  `idtemporada` varchar(150) NOT NULL,
  `descripcion` varchar(160) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`idCategoria`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `detalle`
--
ALTER TABLE `detalle`
  ADD PRIMARY KEY (`iddetalle`),
  ADD KEY `fk_detalle_Producto1_idx` (`idProducto`),
  ADD KEY `fk_detalle_Pedido1_idx` (`idPedido`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`idEmpresa`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`idPedido`),
  ADD KEY `fk_Pedido_Cliente1_idx` (`idCliente`),
  ADD KEY `fk_Pedido_Empresa1_idx` (`idEmpresa`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idProducto`),
  ADD KEY `fk_Producto_Empresa_idx` (`idEmpresa`);

--
-- Indices de la tabla `reportes`
--
ALTER TABLE `reportes`
  ADD PRIMARY KEY (`idReportes`),
  ADD KEY `fk_Reportes_Cliente1_idx` (`idCliente`),
  ADD KEY `fk_Reportes_Empresa1_idx` (`idEmpresa`);

--
-- Indices de la tabla `sucursalgps`
--
ALTER TABLE `sucursalgps`
  ADD PRIMARY KEY (`idSucursal`),
  ADD KEY `fk_SucursalGps_Empresa1_idx` (`idEmpresa`);

--
-- Indices de la tabla `temporada`
--
ALTER TABLE `temporada`
  ADD PRIMARY KEY (`idtemporada`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle`
--
ALTER TABLE `detalle`
  ADD CONSTRAINT `fk_detalle_Pedido1` FOREIGN KEY (`idPedido`) REFERENCES `pedido` (`idPedido`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalle_Producto1` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `fk_Pedido_Cliente1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Pedido_Empresa1` FOREIGN KEY (`idEmpresa`) REFERENCES `empresa` (`idEmpresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_Producto_Empresa` FOREIGN KEY (`idEmpresa`) REFERENCES `empresa` (`idEmpresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `reportes`
--
ALTER TABLE `reportes`
  ADD CONSTRAINT `fk_Reportes_Cliente1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Reportes_Empresa1` FOREIGN KEY (`idEmpresa`) REFERENCES `empresa` (`idEmpresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sucursalgps`
--
ALTER TABLE `sucursalgps`
  ADD CONSTRAINT `fk_SucursalGps_Empresa1` FOREIGN KEY (`idEmpresa`) REFERENCES `empresa` (`idEmpresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
