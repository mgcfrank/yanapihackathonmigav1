<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 *
 */
class Metodos
{
    private $CI;

    public function __construct()
    {
        $this->CI = get_instance();
    }

    public function _resultClient($estado, $mensaje, $result)
    {
        header('Content-Type: application/json; charset=utf8');
        echo json_encode([
            "status"  => $estado,
            "message" => $mensaje,
            "data"    => $result,
        ]);
    }

    //EXTRAS::::::::::::::::::::::::::::::::::
    public function generar_token_seguro($longitud)
    {
        if ($longitud < 4) {$longitud = 4;}
        return bin2hex(random_bytes(($longitud - ($longitud % 2)) / 2));
    }
    public function deleteFile($file)
    {
        if (file_exists($file)) {
            unlink($file);
        }
    }
    public function getURLLiteral($data)
    {
        $imagen_validad = str_replace(array('*', '&', '^', '%', '$', '#', '@', '!', '(', ')', '_', '+', '-', '=', '{', '[', ']', '}', '|', "'", ';', '"', '”', ':', ',', '<', '.', '>', '/', '?', '¿', '¡', '~', '`', '°', 'ñ', 'Ñ', 'á', 'í', 'é', 'ú', 'ó', 'Á', 'É', 'Í', 'Ó', 'Ú'), '', $data);
        return str_replace(' ', '-', $imagen_validad);
    }
    public function generarCodigo($longitud)
    {
        $key     = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max     = strlen($pattern) - 1;
        for ($i = 0; $i < $longitud; $i++) {
            $key .= $pattern{mt_rand(0, $max)};
        }
        return $key;
    }

    //estra
    public function redimensionarImagen($origen, $destino, $ancho, $alto, $jpgCalidad = 100)
    {
        $datos = getimagesize($origen);
        // if ($datos[0] > $ancho || $datos[1] > $alto) {
        if ($datos[2] == 1) {
            $img = imagecreatefromgif($origen);
        }
        if ($datos[2] == 2) {
            $img = imagecreatefromjpeg($origen);
        }
        if ($datos[2] == 3) {
            $img = imagecreatefrompng($origen);
        }
        //redimensionamos proporcionalmente
        if (rad2deg(atan($datos[0] / $datos[1])) > rad2deg(atan($ancho / $alto))) {
            $anchura = $ancho;
            $altura  = round(($datos[1] * $ancho) / $datos[0]);
        } else {
            $altura  = $alto;
            $anchura = round(($datos[0] * $alto) / $datos[1]);
        }
        //creamos la nueva imagen
        $nuevaImagen = imagecreatetruecolor($anchura, $altura);
        //-- esto rellena el color negro por el color seleccionado
        $color = imagecolorallocatealpha($nuevaImagen, 255, 255, 255, 1);
        //-- aquí es para la transparencia del la imagen PNG
        //imagecolortransparent($tmp, $color);
        //-- y al final aquí se elimina el color negro de los bordes
        imagefill($nuevaImagen, 0, 0, $color);
        //redimensionamos la imagen original copiandola en la imgen
        imagecopyresampled($nuevaImagen, $img, 0, 0, 0, 0, $anchura, $altura, $datos[0], $datos[1]);
        // if ($datos[2] == 2) {
        imagejpeg($nuevaImagen, $destino, $jpgCalidad);
        //eliminamos la imagen temporal
        imagedestroy($nuevaImagen);
        return true;
    }

    //estra
    public function redimensionarImagenPng($origen, $destino, $ancho, $alto, $jpgCalidad = 100)
    {
        $datos = getimagesize($origen);
        $img   = imagecreatefrompng($origen);
        //redimensionamos proporcionalmente
        if (rad2deg(atan($datos[0] / $datos[1])) > rad2deg(atan($ancho / $alto))) {
            $anchura = $ancho;
            $altura  = round(($datos[1] * $ancho) / $datos[0]);
        } else {
            $altura  = $alto;
            $anchura = round(($datos[0] * $alto) / $datos[1]);
        }
        //creamos la nueva imagen
        $nuevaImagen = imagecreatetruecolor($anchura, $altura);
        //redimensionamos la imagen original copiandola en la imgen
        imagecopyresampled($nuevaImagen, $img, 0, 0, 0, 0, $anchura, $altura, $datos[0], $datos[1]);
        imagepng($nuevaImagen, $destino);
        //eliminamos la imagen temporal
        imagedestroy($nuevaImagen);
        return true;
    }

    public function resizeIMG($file, $imgpath, $width, $height)
    {
        /* Get original image x y*/
        list($w, $h) = getimagesize($file['tmp_name']);
        /* calculate new image size with ratio */
        $ratio = max($width / $w, $height / $h);
        $h     = ceil($height / $ratio);
        $x     = ($w - $width / $ratio) / 2;
        $w     = ceil($width / $ratio);

        /* new file name */
        $path = $imgpath;
        /* read binary data from image file */
        $imgString = file_get_contents($file['tmp_name']);
        /* create image from string */
        $image = imagecreatefromstring($imgString);
        $tmp   = imagecreatetruecolor($width, $height);
        imagecopyresampled($tmp, $image, 0, 0, $x, 0, $width, $height, $w, $h);
        /* Save image */
        switch ($file['type']) {
            case 'image/jpeg':
                imagejpeg($tmp, $path, 100);
                break;
            case 'image/png':
                imagepng($tmp, $path, 0);
                break;
            case 'image/gif':
                imagegif($tmp, $path);
                break;
            default:
                //exit;
                break;
        }
        return $path;

        /* cleanup memory */
        imagedestroy($image);
        imagedestroy($tmp);
    }

    public function getBoundaries($lat, $lng, $distance = 1, $earthRadius = 6371)
    {
        $return = array();
        // Los angulos para cada dirección
        $cardinalCoords = array('north' => '0',
            'south'                         => '180',
            'east'                          => '90',
            'west'                          => '270');

        $rLat     = deg2rad($lat);
        $rLng     = deg2rad($lng);
        $rAngDist = $distance / $earthRadius;

        foreach ($cardinalCoords as $name => $angle) {
            $rAngle = deg2rad($angle);
            $rLatB  = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
            $rLonB  = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));

            $return[$name] = array('lat' => (float) rad2deg($rLatB),
                'lng'                        => (float) rad2deg($rLonB));
        }

        return array('min_lat' => $return['south']['lat'],
            'max_lat'              => $return['north']['lat'],
            'min_lng'              => $return['west']['lng'],
            'max_lng'              => $return['east']['lng']);
    }
}
