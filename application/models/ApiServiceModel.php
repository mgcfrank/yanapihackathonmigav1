<?php
class ApiServiceModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getLoginEmpresa($numero)
    {
        $sql = $this->db->query("SELECT idEmpresa, nombre, descripcion, estado, foto, numero, precio_envio_metro FROM empresa WHERE numero='$numero'");
        return $sql->row_array();
    }

    public function getEmpresaExist($idEmpresa)
    {
        $sql = $this->db->query("SELECT * FROM empresa where idEmpresa='$idEmpresa'");
        return (count($sql->result()) > 0) ? true : false;
    }

    public function getExistEmpresaCelular($celular)
    {
        $sql = $this->db->query("SELECT * FROM empresa where numero='$celular'");
        return (count($sql->result()) > 0) ? true : false;
    }

    public function setEmpresa($data)
    {
        return $this->db->insert('empresa', $data);
    }

    public function updateEmpresa($data)
    {
        $this->db->where('idEmpresa', $data['idEmpresa']);
        return $this->db->update('empresa', $data);
    }

    public function setSucursalGps($data)
    {
        return $this->db->insert('sucursalgps', $data);
    }

    public function updateSucursalEmpresa($data)
    {
        $this->db->where('idSucursal', $data['idSucursal']);
        return $this->db->update('sucursalgps', $data);
    }

    public function getSucursalesEmpresa($idEmpresa)
    {
        $sql = $this->db->query("SELECT *
                FROM sucursalgps where idEmpresa = '$idEmpresa'
                ORDER BY idEmpresa DESC");
        return $sql->result();
    }

    public function getEmpresaGpsCliente($filtro, $lat, $lng, $box, $distance)
    {
        switch ($filtro) {
            case 'Todos':
                $sql = $this->db->query('SELECT c.idSucursal, c.idEmpresa, c.direccion, c.latitud, c.longitud, e.categorias, ( 6371 * ACOS(COS(RADIANS(' . $lat . '))* COS(RADIANS(c.latitud))* COS(RADIANS(c.longitud)- RADIANS(' . $lng . '))+ SIN( RADIANS(' . $lat . ') ) * SIN(RADIANS(c.latitud)))) AS distance
                     FROM sucursalgps c, empresa e
                     WHERE c.idEmpresa =e.idEmpresa
                     AND (c.latitud BETWEEN ' . $box['min_lat'] . ' AND ' . $box['max_lat'] . ')
                     AND (c.longitud BETWEEN ' . $box['min_lng'] . ' AND ' . $box['max_lng'] . ')
                     HAVING distance < ' . $distance . '
                     ORDER BY distance ASC');
                break;

            default:
                $sql = $this->db->query("SELECT c.idSucursal, c.idEmpresa, c.direccion, c.latitud, c.longitud, e.categorias, ( 6371 * ACOS(COS(RADIANS(" . $lat . "))* COS(RADIANS(c.latitud))* COS(RADIANS(c.longitud)- RADIANS(" . $lng . "))+ SIN( RADIANS(" . $lat . ") ) * SIN(RADIANS(c.latitud)))) AS distance
                     FROM sucursalgps c, empresa e
                     WHERE c.idEmpresa =e.idEmpresa
                     AND UPPER(e.categorias) like UPPER('%$filtro%')
                     AND (c.latitud BETWEEN " . $box['min_lat'] . " AND " . $box['max_lat'] . ")
                     AND (c.longitud BETWEEN " . $box['min_lng'] . " AND " . $box['max_lng'] . ")
                     HAVING distance < " . $distance . "
                     ORDER BY distance ASC");
                break;
        }

        return $sql->result();
    }

    public function getVistaEmpresaCliente($idEmpresa, $idSucursal)
    {
        $sql     = $this->db->query("SELECT idEmpresa, nombre, descripcion, numero, whatsapp, messenger, foto, precio_envio_metro, delivery, mayorista, minorista, estado FROM empresa WHERE idEmpresa='$idEmpresa'");
        $empresa = $sql->row_array();

        $sql      = $this->db->query("SELECT horario_atencion, horario_pedidos, direccion FROM sucursalgps WHERE idSucursal='$idSucursal'");
        $sucursal = $sql->result();

        $sql       = $this->db->query("SELECT idProducto, titulo, descripcion, precio, stock, foto FROM producto WHERE idEmpresa='$idEmpresa' ORDER BY RAND() LIMIT 5");
        $productos = $sql->result();

        return array(
            'empresa'   => $empresa,
            'sucursal'  => $sucursal,
            'productos' => $productos,
        );
    }

    public function getBusquedaEmpresa($txtBuscar, $latitud, $longitud)
    {
        $arrayTxtBuscar  = explode(' ', $txtBuscar);
        $categoriaAuxSql = "";
        for ($i = 0; $i < count($arrayTxtBuscar); $i++) {
            if (strlen($arrayTxtBuscar[$i]) > 2) {
                $categoriaAuxSql = $categoriaAuxSql . " UPPER(e.categorias) like UPPER('%$arrayTxtBuscar[$i]%') OR UPPER(e.descripcion) like UPPER('%$arrayTxtBuscar[$i]%')  OR UPPER(e.nombre) like UPPER('%$arrayTxtBuscar[$i]%')  OR UPPER(c.direccion) like UPPER('%$arrayTxtBuscar[$i]%') OR";}
        }
        $categoriaAuxSql .= " UPPER(e.categorias) like UPPER('%$txtBuscar%') OR UPPER(e.descripcion) like UPPER('%$txtBuscar%')  OR UPPER(e.nombre) like UPPER('%$txtBuscar%')  OR UPPER(c.direccion) like UPPER('%$txtBuscar%')";
        // $categoriaAuxSql=substr($categoriaAuxSql,0,strlen($categoriaAuxSql)-2);
        $sql = $this->db->query("SELECT c.idSucursal, c.idEmpresa, e.nombre, e.foto, e.mayorista, e.minorista, c.direccion, c.latitud, c.longitud, e.categorias, (6371 * ACOS(COS(RADIANS($latitud))* COS(RADIANS(c.latitud))* COS(RADIANS(c.longitud)- RADIANS($longitud))+ SIN( RADIANS($latitud) ) * SIN(RADIANS(c.latitud)))) AS distance
                     FROM sucursalgps c, empresa e
                     WHERE c.idEmpresa =e.idEmpresa
            and ($categoriaAuxSql)
            ORDER BY distance ASC");

        return $sql->result();
    }

    //:::::::::::::::::::::::::::::::::::::::
    //::::::    PRODUCTOS
    //:::::::::::::::::::::::::::::::::::::::
    public function getProductosEmpresa($filtro, $idEmpresa)
    {
        switch ($filtro) {
            case 'Todos':
                $sql = $this->db->query("SELECT idProducto, titulo, descripcion, precio, stock, foto, 0 as carro FROM producto WHERE idEmpresa='$idEmpresa' ORDER BY f_modificacion DESC");
                break;

            case 'Stock':
                $sql = $this->db->query("SELECT idProducto, titulo, descripcion, precio, stock, foto, 0 as carro FROM producto WHERE idEmpresa='$idEmpresa' and stock<=5 ORDER BY f_modificacion DESC");
                break;

            case 'Mas vendidos':
                $sql = $this->db->query("SELECT p.idProducto, p.titulo, p.descripcion, p.precio, p.stock, p.foto, 0 as carro
                    FROM producto p, detalle d
                    where p.idProducto=d.idProducto
                    and p.idEmpresa='$idEmpresa'
                    GROUP BY d.idProducto
                    ORDER by SUM(d.cantidad) DESC limit 50");
                break;
            case 'Menos vendidos':
                $sql = $this->db->query("SELECT p.idProducto, p.titulo, p.descripcion, p.precio, p.stock, p.foto, 0 as carro
                    FROM producto p, detalle d
                    where p.idProducto=d.idProducto
                    and p.idEmpresa='$idEmpresa'
                    GROUP BY d.idProducto
                    ORDER by SUM(d.cantidad) ASC limit 50");
                break;
        }
        return $sql->result();
    }

    public function setProductoEmpresa($data)
    {
        return $this->db->insert('producto', $data);
    }

    //::::::::::::::::::::::::::::::::::::::::::
    //::::::::::       PEDIDOS
    //:::::::::::::::::::::::::::::::::::::::::::
    public function getPedidosEmpresa($filtro, $idEmpresa)
    {
        switch ($filtro) {
            case 'Hoy':
                $sql = $this->db->query("SELECT p.idPedido, CONCAT(c.nombres, ' ', c.apellidos) as cliente, p.precio_pedido, p.f_alta, p.estado
                    FROM pedido p, cliente c
                    WHERE p.idCliente=c.idCliente
                    and p.idEmpresa='$idEmpresa'
                    and DATE_FORMAT(p.f_alta,'%Y-%m-%d')='" . date('Y-m-d') . "'");
                break;

            case 'Ayer':
                $sql = $this->db->query("SELECT p.idPedido, CONCAT(c.nombres, ' ', c.apellidos) as cliente, p.precio_pedido, p.f_alta, p.estado
                    FROM pedido p, cliente c
                    WHERE p.idCliente=c.idCliente
                    and p.idEmpresa='$idEmpresa'
                    and DATE_FORMAT(p.f_alta,'%Y-%m-%d')='" . date('Y-m-d', strtotime('-1 day')) . "'");
                break;

            case 'Mes':
                $sql = $this->db->query("SELECT p.idPedido, CONCAT(c.nombres, ' ', c.apellidos) as cliente, p.precio_pedido, p.f_alta, p.estado
                    FROM pedido p, cliente c
                    WHERE p.idCliente=c.idCliente
                    and p.idEmpresa='$idEmpresa'
                    and MONTH(p.f_alta) = MONTH(CURRENT_DATE())
                    and YEAR(p.f_alta) = YEAR(CURRENT_DATE())");
                break;
            default: //todos
                $sql = $this->db->query("SELECT p.idPedido, CONCAT(c.nombres, ' ', c.apellidos) as cliente, p.precio_pedido, p.f_alta, p.estado
                    FROM pedido p, cliente c
                    WHERE p.idCliente=c.idCliente
                    and p.idEmpresa='$idEmpresa'");
                break;
        }
        return $sql->result();
    }

    public function getPedidosIdEmpresa($idPedido)
    {
        $sql = $this->db->query("SELECT CONCAT(c.nombres, ' ', c.apellidos) as cliente, c.numero, p.precio_pedido, p.f_alta, p.estado
                    FROM pedido p, cliente c
                    WHERE p.idCliente=c.idCliente
                    and p.idPedido='$idPedido'");
        $pedido = $sql->row_array();

        $sql = $this->db->query("SELECT p.titulo, p.foto, d.cantidad, d.precio_actual
                    FROM detalle d, producto p
                    WHERE d.idProducto=p.idProducto
                    and d.idPedido='$idPedido'");
        $detalle = $sql->result();

        return array(
            'pedido'  => $pedido,
            'detalle' => $detalle,
        );
    }

    public function getPedidosCliente($filtro, $idCliente)
    {
        switch ($filtro) {
            case 'Hoy':
                $sql = $this->db->query("SELECT p.idPedido, c.nombre as empresa, p.precio_pedido, p.f_alta, p.estado
                    FROM pedido p, empresa c
                    WHERE p.idEmpresa=c.idEmpresa
                    and p.idCliente='$idCliente'
                    and DATE_FORMAT(p.f_alta,'%Y-%m-%d')='" . date('Y-m-d') . "'");
                break;

            case 'Ayer':
                $sql = $this->db->query("SELECT p.idPedido, c.nombre as empresa, p.precio_pedido, p.f_alta, p.estado
                FROM pedido p, empresa c
                WHERE p.idEmpresa = c.idEmpresa
                and p.idCliente = '$idCliente'
                and DATE_FORMAT(p.f_alta, '%Y-%m-%d') = '" . date('Y-m-d', strtotime('-1 day')) . "'");
                break;

            case 'Mes':
                $sql = $this->db->query("SELECT p.idPedido, c.nombre as empresa, p.precio_pedido, p.f_alta, p.estado
                FROM pedido p, empresa c
                WHERE p.idEmpresa = c.idEmpresa
                and p.idCliente = '$idCliente'
                and MONTH(p.f_alta) = MONTH(CURRENT_DATE())
                and YEAR(p.f_alta)  = YEAR(CURRENT_DATE())");
                break;
            default: //todos
                $sql = $this->db->query("SELECT p.idPedido, c.nombre as empresa, p.precio_pedido, p.f_alta, p.estado
                FROM pedido p, empresa c
                WHERE p.idEmpresa = c.idEmpresa
                and p.idCliente = '$idCliente'");
                break;
        }
        return $sql->result();
    }

    public function getPedidosIdCliente($idPedido)
    {
        $sql = $this->db->query("SELECT c.nombre as empresa, c.numero, c.whatsapp, p.precio_pedido, p.f_alta, p.estado
                FROM pedido p, empresa c
                WHERE p.idEmpresa = c.idEmpresa
                and p.idPedido = '$idPedido'");
        $pedido = $sql->row_array();

        $sql = $this->db->query("SELECT p.titulo, p.foto, d.cantidad, d.precio_actual
                FROM detalle d, producto p
                WHERE d.idProducto = p.idProducto
                and d.idPedido = '$idPedido'");
        $detalle = $sql->result();

        return array(
            'pedido'  => $pedido,
            'detalle' => $detalle,
        );
    }

    public function setPedidoCliente($data)
    {
        return $this->db->insert('pedido', $data);
    }

    public function setDetallePedido($data)
    {
        return $this->db->insert('detalle', $data);
    }

    // ::::::::::::::::::::::::::::::::::::::::::::::::::
    // ::::::::      CLIENTE
    //:::::::::::::::::::::::::::::::::::::::::::::::::::

    public function getLoginCliente($numero)
    {
        $sql = $this->db->query("SELECT idCliente, foto, nombres, apellidos, numero, ci, estado FROM cliente WHERE numero = '$numero'");
        return $sql->row_array();
    }

    public function getExistClienteCelular($celular)
    {
        $sql = $this->db->query("SELECT * FROM cliente where numero = '$celular'");
        return (count($sql->result()) > 0) ? true : false;
    }

    public function setCliente($data)
    {
        return $this->db->insert('cliente', $data);
    }

    ///////////////////////////////////////detalle empresa
    public function getUrlSlugExistEmpresaDetalle($titulo)
    {
        $sql = $this->db->query("SELECT * FROMdetalle_empresawhereurl = '$titulo'");
        return (count($sql->result()) > 0) ? true : false;
    }

    public function getExistEmpresaDetalle($idusuario)
    {
        $sql = $this->db->query("SELECT * FROMdetalle_empresawhereidusuario = '$idusuario'");
        return (count($sql->result()) > 0) ? true : false;
    }

    public function setDetalleEmpresa($data)
    {
        return $this->db->insert('detalle_empresa', $data);
    }

    public function updateDetalleEmpresa($data)
    {
        $this->db->where('idusuario', $data['idusuario']);
        return $this->db->update('detalle_empresa', $data);
    }

    public function getContactoEmpresa($idusuario)
    {
        $sql = $this->db->query("SELECTtelefonos, facebook, web, whatsapp, correo, instagramfromdetalle_empresawhereidusuario = '$idusuario'");
        return $sql->row_array();
    }

    public function getInformacionEmpresa($idusuario)
    {
        $sql = $this->db->query("SELECTcategoria, dias_atencion, foto_portada, descripcionfromdetalle_empresawhereidusuario = '$idusuario'");
        return $sql->row_array();
    }

    public function getConfigDeliveryEmpresa($idusuario)
    {
        $sql = $this->db->query("SELECTcomision_100metros, rango_mapa_servicio, img_markerfromdetalle_empresawhereidusuario = '$idusuario'");
        return $sql->row_array();
    }

    public function getSucursalesDirecionGps($idusuario)
    {
        $sql = $this->db->query("SELECTidDireccionGps, latitud, longitud, descripcion
                FROMdirecion_gpswhereidusuario = '$idusuario'
                ORDERBYidDireccionGpsDESC");
        return $sql->result();
    }

    public function setSucursalesDirecionGps($data)
    {
        return $this->db->insert('direcion_gps', $data);
    }

    public function updateSucursalesDirecionGps($data)
    {
        $this->db->where('idDireccionGps', $data['idDireccionGps']);
        return $this->db->update('direcion_gps', $data);
    }
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    //::::::::::::::   publicacion usuario
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    public function getUrlSlugExistPublicacion($titulo)
    {
        $sql = $this->db->query("SELECT * FROMpublicacionwhereurl = '$titulo'");
        return (count($sql->result()) > 0) ? true : false;
    }

    public function setPublicacion($data)
    {
        return $this->db->insert('publicacion', $data);
    }

    public function getPublicacionesUsuario($idusuario)
    {
        $sql = $this->db->query("SELECTs . idpublicacion, s . titulo, substring(s . descripcion, 1, 20) as descripcion, s . imagenes, s . tipo_publicacion, s . stock, s . estado, s . estado_detalle
                FROMpublicacions, usuariou
                wheres . idusuario = u . idusuario
                and u . estado != 'Restringido'
                and u . idusuario = '$idusuario'
                ORDERBYs . f_altaDESC");
        return $sql->result();
    }

    public function getPublicacionUsuario($idpublicacion)
    {
        $sql = $this->db->query("SELECT * FROMpublicacionwhereidpublicacion = '$idpublicacion'ORDERBYf_altaDESC");
        return $sql->row_array();
    }

    public function updatePublicacion($data)
    {
        $this->db->where('idpublicacion', $data['idpublicacion']);
        return $this->db->update('publicacion', $data);
    }

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    //::::::::::::::   publicacion de muro
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    public function getPublicacionesPagaCiudad($ciudad, $limite, $etiqueta)
    {
        //busca publicidad por ciudad
        $sql = $this->db->query("SELECTs . idpublicacion, u . nombre as empresa, u . foto as fotoempresa, s . titulo as titulo, substring(s . descripcion, 1, 20) as descripcion, s . imagenes as imagenes, s . categoria as categoria, s . precio as precio, '$etiqueta' as etiqueta
                FROMpublicacions, usuariou
                wheres . estado = 'PUBLICIDAD'
                and s . ciudadeslike'%$ciudad%'
                and s . idusuario = u . idusuario
                and u . estado != 'Restringido'
                ORDERBYRAND()LIMIT $limite");
        return $sql->result();
    }

    public function getPublicacionesPaga($limite, $etiqueta)
    {
        //busca publicidad general
        $sql = $this->db->query("SELECTs . idpublicacion, d . nombre_empresa as empresa, u . foto as fotoempresa, s . titulo as titulo, substring(s . descripcion, 1, 20) as descripcion, s . imagenes as imagenes, s . categoria as categoria, s . modalidad_publicacion, s . tipo_publicacion, '$etiqueta' as etiqueta
                FROMpublicacions, usuariou, detalle_empresad
                wheres . estado = 'PUBLICIDAD'
                and s . idusuario = u . idusuario
                and d . idusuario = u . idusuario
                and u . estado != 'Restringido'
                ORDERBYRAND()LIMIT $limite");
        return $sql->result();
    }

    public function getPublicacionesCiudad($ciudad, $limite, $etiqueta)
    {
        //busca publicidad por ciudad
        $sql = $this->db->query("SELECTs . idpublicacion, d . nombre_empresa as empresa, u . foto as fotoempresa, s . titulo as titulo, substring(s . descripcion, 1, 20) as descripcion, s . imagenes as imagenes, s . categoria as categoria, s . modalidad_publicacion, s . tipo_publicacion, '$etiqueta' as etiqueta
                FROMpublicacions, usuariou, detalle_empresad
                where(s . estado != 'Restringido' and s . estado != 'Espera')
                and s . ciudadeslike'%$ciudad%'
                and s . idusuario = u . idusuario
                and d . idusuario = u . idusuario
                and u . estado != 'Restringido'
                ORDERBYRAND()LIMIT $limite");
        return $sql->result();
    }

    public function getPublicacionesGenerales($limite, $etiqueta)
    {
        //busca publicidad por ciudad
        $sql = $this->db->query("SELECTs . idpublicacion, d . nombre_empresa as empresa, u . foto as fotoempresa, s . titulo as titulo, substring(s . descripcion, 1, 20) as descripcion, s . imagenes as imagenes, s . categoria as categoria, s . modalidad_publicacion, s . tipo_publicacion, '$etiqueta' as etiqueta
                FROMpublicacions, usuariou, detalle_empresad
                where(s . estado != 'Restringido' and s . estado != 'Espera')
                and s . idusuario = u . idusuario
                and d . idusuario = u . idusuario
                and u . estado != 'Restringido'
                ORDERBYRAND()LIMIT $limite");
        return $sql->result();
    }

    public function getPublicacionesCiudadCategoria($ciudad, $categoria, $limite, $etiqueta)
    {
        //genera consulta faltante
        $arrayCategoria = explode('_', $categoria);
        $cadena_aux     = "";
        for ($i = 0; $i < count($arrayCategoria); $i++) {
            $cadena_aux = $cadena_aux . "s . categorialike'%$arrayCategoria[$i]%' or ";
        }
        $cadena_aux = substr($cadena_aux, 0, strlen($cadena_aux) - 2);
        //busca publicidad por ciudad
        $sql = $this->db->query("SELECTs . idpublicacion, d . nombre_empresa as empresa, u . foto as fotoempresa, s . titulo as titulo, substring(s . descripcion, 1, 20) as descripcion, s . imagenes as imagenes, s . categoria as categoria, s . modalidad_publicacion, s . tipo_publicacion, '$etiqueta' as etiqueta
                FROMpublicacions, usuariou, detalle_empresad
                where(s . estado != 'Restringido' and s . estado != 'Espera')
                and s . ciudadeslike'%$ciudad%'
                and s . idusuario = u . idusuario
                and d . idusuario = u . idusuario
                and u . estado != 'Restringido'
                and ($cadena_aux)
                ORDERBYRAND()LIMIT $limite");
        return $sql->result();
    }

    public function getPublicacionesGeneralCategoria($categoria, $limite, $etiqueta)
    {
        //genera consulta faltante
        $arrayCategoria = explode('_', $categoria);
        $cadena_aux     = "";
        for ($i = 0; $i < count($arrayCategoria); $i++) {
            $cadena_aux = $cadena_aux . "s . categorialike'%$arrayCategoria[$i]%' or ";
        }
        $cadena_aux = substr($cadena_aux, 0, strlen($cadena_aux) - 2);
        //busca publicidad por ciudad
        $sql = $this->db->query("SELECTs . idpublicacion, d . nombre_empresa as empresa, u . foto as fotoempresa, s . titulo as titulo, substring(s . descripcion, 1, 20) as descripcion, s . imagenes as imagenes, s . categoria as categoria, s . modalidad_publicacion, s . tipo_publicacion, '$etiqueta' as etiqueta
                FROMpublicacions, usuariou, detalle_empresad
                where(s . estado != 'Restringido' and s . estado != 'Espera')
                and s . idusuario = u . idusuario
                and d . idusuario = u . idusuario
                and u . estado != 'Restringido'
                and ($cadena_aux)
                ORDERBYRAND()LIMIT $limite");
        return $sql->result();
    }

    public function getPublicacionesSemanal($ciudad, $categoria, $semana, $limite, $etiqueta)
    {
        //genera consulta faltante
        $arrayCategoria = explode('_', $categoria);
        $cadena_aux     = "";
        for ($i = 0; $i < count($arrayCategoria); $i++) {
            $cadena_aux = $cadena_aux . "s . categorialike'%$arrayCategoria[$i]%' or ";
        }
        $cadena_aux = substr($cadena_aux, 0, strlen($cadena_aux) - 2);
        //busca publicidad por ciudad
        $sql = $this->db->query("SELECTs . idpublicacion, d . nombre_empresa as empresa, u . foto as fotoempresa, s . titulo as titulo, substring(s . descripcion, 1, 20) as descripcion, s . imagenes as imagenes, s . categoria as categoria, s . modalidad_publicacion, s . tipo_publicacion, '$etiqueta' as etiqueta
                FROMpublicacions, usuariou, detalle_empresad
                where(s . estado != 'Restringido' and s . estado != 'Espera')
                and s . ciudadeslike'%$ciudad%'
                and s . semanales_diaslike'%$semana%'
                and s . modalidad_publicacion = 'Semanal'
                and s . idusuario = u . idusuario
                and d . idusuario = u . idusuario
                and u . estado != 'Restringido'
                and ($cadena_aux)
                ORDERBYRAND()LIMIT $limite");
        $_FILAS         = $sql->result();
        $_PUBLICACIONES = $_FILAS;
        if (count($_FILAS) < $limite) {
            //busca ofertas semanales fe forma general
            $sql = $this->db->query("SELECTs . idpublicacion, d . nombre_empresa as empresa, u . foto as fotoempresa, s . titulo as titulo, substring(s . descripcion, 1, 20) as descripcion, s . imagenes as imagenes, s . categoria as categoria, s . modalidad_publicacion, s . tipo_publicacion, '$etiqueta' as etiqueta
                FROMpublicacions, usuariou, detalle_empresad
                where(s . estado != 'Restringido' and s . estado != 'Espera')
                and s . semanales_diaslike'%$semana%'
                and s . modalidad_publicacion = 'Semanal'
                and s . idusuario = u . idusuario
                and d . idusuario = u . idusuario
                and u . estado != 'Restringido'
                ORDERBYRAND()LIMIT" . ($limite - count($_FILAS)));
            $_FILAS         = $sql->result();
            $_PUBLICACIONES = array_merge($_PUBLICACIONES, $_FILAS);
        }
        return $_PUBLICACIONES;
    }

    public function getPublicacionesOferta($ciudad, $categoria, $limite, $etiqueta)
    {
        //BUSCA OFERTAS SEGUN LA CIUDAD Y GUSTOS DEL CLIENTE
        $arrayCategoria = explode('_', $categoria);
        $cadena_aux     = "";
        for ($i = 0; $i < count($arrayCategoria); $i++) {
            $cadena_aux = $cadena_aux . "s . categorialike'%$arrayCategoria[$i]%' or ";
        }
        $cadena_aux = substr($cadena_aux, 0, strlen($cadena_aux) - 2);
        //busca publicidad por ciudad
        $sql = $this->db->query("SELECTs . idpublicacion, d . nombre_empresa as empresa, u . foto as fotoempresa, s . titulo as titulo, substring(s . descripcion, 1, 20) as descripcion, s . imagenes as imagenes, s . categoria as categoria, s . modalidad_publicacion, s . tipo_publicacion, '$etiqueta' as etiqueta
                FROMpublicacions, usuariou, detalle_empresad
                where(s . estado != 'Restringido' and s . estado != 'Espera')
                and s . ciudadeslike'%$ciudad%'
                and s . modalidad_publicacion = 'Oferta'
                and s . idusuario = u . idusuario
                and d . idusuario = u . idusuario
                and u . estado != 'Restringido'
                and ($cadena_aux)
                ORDERBYRAND()LIMIT $limite");
        $_FILAS         = $sql->result();
        $_PUBLICACIONES = $_FILAS;
        if (count($_FILAS) < $limite) {
            //busca ofertas de forma general
            $sql = $this->db->query("SELECTs . idpublicacion, d . nombre_empresa as empresa, u . foto as fotoempresa, s . titulo as titulo, substring(s . descripcion, 1, 20) as descripcion, s . imagenes as imagenes, s . categoria as categoria, s . modalidad_publicacion, s . tipo_publicacion, '$etiqueta' as etiqueta
                FROMpublicacions, usuariou, detalle_empresad
                where(s . estado != 'Restringido' and s . estado != 'Espera')
                and s . modalidad_publicacion = 'Oferta'
                and s . idusuario = u . idusuario
                and d . idusuario = u . idusuario
                and u . estado != 'Restringido'
                ORDERBYRAND()LIMIT" . ($limite - count($_FILAS)));
            $_FILAS         = $sql->result();
            $_PUBLICACIONES = array_merge($_PUBLICACIONES, $_FILAS);
        }
        return $_PUBLICACIONES;
    }

    public function getVistaTemporada($temporada, $ciudad, $limite, $etiqueta)
    {
        //BUSCA OFERTAS SEGUN LA CIUDAD Y GUSTOS DEL CLIENTE
        $arrayCategoria = explode('_', $temporada);
        $cadena_aux     = "";
        for ($i = 0; $i < count($arrayCategoria); $i++) {
            $cadena_aux = $cadena_aux . "s . categorialike'%$arrayCategoria[$i]%' or s . titulolike'%$arrayCategoria[$i]%' or s . descripcionlike'%$arrayCategoria[$i]%' or ";
        }
        $cadena_aux = substr($cadena_aux, 0, strlen($cadena_aux) - 2);
        //busca publicidad por ciudad
        $sql = $this->db->query("SELECTs . idpublicacion, d . nombre_empresa as empresa, u . foto as fotoempresa, s . titulo as titulo, substring(s . descripcion, 1, 20) as descripcion, s . imagenes as imagenes, s . categoria as categoria, s . modalidad_publicacion, s . tipo_publicacion, '$etiqueta' as etiqueta
                FROMpublicacions, usuariou, detalle_empresad
                where(s . estado != 'Restringido' and s . estado != 'Espera')
                and s . ciudadeslike'%$ciudad%'
                and s . idusuario = u . idusuario
                and d . idusuario = u . idusuario
                and u . estado != 'Restringido'
                and ($cadena_aux)
                ORDERBYRAND()LIMIT $limite");
        $_FILAS         = $sql->result();
        $_PUBLICACIONES = $_FILAS;
        if (count($_FILAS) < $limite) {
            //busca temporada de forma general
            $sql = $this->db->query("SELECTs . idpublicacion, d . nombre_empresa as empresa, u . foto as fotoempresa, s . titulo as titulo, substring(s . descripcion, 1, 20) as descripcion, s . imagenes as imagenes, s . categoria as categoria, s . modalidad_publicacion, s . tipo_publicacion, '$etiqueta' as etiqueta
                FROMpublicacions, usuariou, detalle_empresad
                where(s . estado != 'Restringido' and s . estado != 'Espera')
                and s . idusuario = u . idusuario
                and d . idusuario = u . idusuario
                and u . estado != 'Restringido'
                and ($cadena_aux)
                ORDERBYRAND()LIMIT" . ($limite - count($_FILAS)));
            $_FILAS         = $sql->result();
            $_PUBLICACIONES = array_merge($_PUBLICACIONES, $_FILAS);
        }
        return $_PUBLICACIONES;
    }

    public function getPublicacionMuro($idpublicacion)
    {
        $sql = $this->db->query("SELECTd . nombre_empresa as empresa, u . foto as fotoempresa, s . tipo_publicacion, s . stock, s . modalidad_publicacion, s . semanales_dias, s . fecha_inicio_publicacion, s . fecha_fin_publicacion, s . titulo, s . descripcion, s . imagenes, s . categoria, s . url, s . ciudades, s . precio, s . tags, u . idusuario
                FROMpublicacions, usuariou, detalle_empresad
                wheres . estado != 'Restringido'
                and s . estado != 'Espera'
                and s . idusuario = u . idusuario
                and d . idusuario = u . idusuario
                and u . estado != 'Restringido'
                and s . idpublicacion = '$idpublicacion'");
        $publicacion = $sql->row_array();

        $sql         = $this->db->query("SELECTu . nombre, u . foto, c . calificacion, c . descripcionFROMcomentarioc, usuariouWHEREc . idusuario = u . idusuario and c . idpublicacion = '$idpublicacion'orderbyc . f_modificaciondesclimit3");
        $comentarios = $sql->result();

        $sql        = $this->db->query("SELECTIFNULL((SUM(calificacion) / COUNT(calificacion)), 5) as puntajeFROMcomentarioWHEREidpublicacion = '$idpublicacion'");
        $puntuacion = $sql->row_array();

        $idusuario    = $publicacion['idusuario'];
        $sql          = $this->db->query("SELECTlatitud, longitud, descripcionFROMdirecion_gpsWHEREidusuario = '$idusuario'");
        $direcion_gps = $sql->result();
//dias_atencion
        $sql       = $this->db->query("SELECTtelefonos, facebook, whatsapp, web, correo, instagram, dias_atencionFROMdetalle_empresaWHEREidusuario = '$idusuario'");
        $contactos = $sql->row_array();

        return array(
            'publicacion'  => $publicacion,
            'comentarios'  => $comentarios,
            'puntuacion'   => $puntuacion,
            'direcion_gps' => $direcion_gps,
            'contactos'    => $contactos,
        );
    }

    //comentario
    public function getExistComentarioUsuarioPublicacion($idusuario, $idpublicacion)
    {
        $sql = $this->db->query("SELECT * FROMcomentariowhereidusuario = '$idusuario' and idpublicacion = '$idpublicacion'");
        return (count($sql->result()) > 0) ? true : false;
    }

    public function setComentario($data)
    {
        return $this->db->insert('comentario', $data);
    }
}
