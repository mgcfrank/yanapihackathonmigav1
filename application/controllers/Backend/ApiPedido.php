<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('America/La_Paz'); #Línea agregada
class ApiPedido extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('ApiServiceModel');
        $this->load->library('Metodos');
    }

    public function index()
    {
        $this->load->view("lista");
        //     echo "<p>" . date('Y-m-d', strtotime('yesterday')) . "::" . date('Y-m-d', strtotime('yesterday')) . "</p>";
        //     echo "<p>" . date('Y-m-d', strtotime('-1 day')) . "::" . date('Y-m-d', strtotime('-1 day')) . "</p>";
    }

    public function setPedidoCliente()
    {
        //oculta las advertencias de codeingiter pra temp
        error_reporting(0);
        ini_set('display_errors', 0);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $columnas = array(
                'idPedido'       => 'p' . date('Ymd-His', time()) . '-' . $this->metodos->generarCodigo(10),
                'titulo'         => $_POST['titulo'],
                'precio_envio'   => str_replace(',', '.', ((string) $_POST['precio_envio'])),
                'precio_pedido'  => str_replace(',', '.', ((string) $_POST['precio_pedido'])),
                'estado'         => 'En espera',
                'direccion'      => $_POST['direccion'],
                'latitud'        => $_POST['latitud'],
                'longitud'       => $_POST['longitud'],
                'f_alta'         => date('Y-m-d H:i:s', time()),
                'f_modificacion' => date('Y-m-d H:i:s', time()),
                'idCliente'      => $_POST['idCliente'],
                'idEmpresa'      => $_POST['idEmpresa'],
            );
            //inserta pedido
            $filas = $this->ApiServiceModel->setPedidoCliente($columnas);
            if (isset($filas)) {

            } else {
                $this->metodos->_resultClient(false, "Red inestable, no se envio su pedido vuelva a intentarlo mas tarde . ", $filas);
                return;
            }
            //inserta detalle
            $data = json_decode($_POST['detalle']);
            foreach ($data as $key => $value) {
                $columnasDetalle = array(
                    'iddetalle'       => 'p' . date('Ymd-His', time()) . '-' . $this->metodos->generarCodigo(10),
                    'idProducto'      => $value->idProducto,
                    'idPedido'        => $columnas['idPedido'],
                    'cantidad'        => $value->catidad,
                    'titulo_producto' => $value->titulo_producto,
                    'precio_actual'   => str_replace(',', '.', ((string) $value->precio_actual)),
                    'f_alta'          => date('Y-m-d H:i:s', time()),
                );
                $filas = $this->ApiServiceModel->setDetallePedido($columnasDetalle);
                if (isset($filas)) {
                } else {
                    return;
                }
            }

            $this->metodos->_resultClient(true, "Pedido enviado exitosamente", $columnas);
        }
    }
    public function getPedidosEmpresa()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $filas = $this->ApiServiceModel->getPedidosEmpresa($_POST['filtro'], $_POST['idEmpresa']);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Lista de pedidos: " . $_POST['filtro'], $filas);
            } else {
                $this->metodos->_resultClient(false, "No hay pedidos: " . $_POST['filtro'], $filas);
            }
        }
    }

    public function getPedidosIdEmpresa()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $filas = $this->ApiServiceModel->getPedidosIdEmpresa($_POST['idPedido']);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Detalle de pedidos: ", $filas);
            } else {
                $this->metodos->_resultClient(false, "No hay detalle: ", $filas);
            }
        }
    }

    public function getPedidosCliente()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $filas = $this->ApiServiceModel->getPedidosCliente($_POST['filtro'], $_POST['idCliente']);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Lista de pedidos: " . $_POST['filtro'], $filas);
            } else {
                $this->metodos->_resultClient(false, "No hay pedidos: " . $_POST['filtro'], $filas);
            }
        }
    }
    public function getPedidosIdCliente()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $filas = $this->ApiServiceModel->getPedidosIdCliente($_POST['idPedido']);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Detalle de pedidos: ", $filas);
            } else {
                $this->metodos->_resultClient(false, "No hay detalle: ", $filas);
            }
        }
    }

    public function setDetallePedido()
    {
        //oculta las advertencias de codeingiter pra temp
        error_reporting(0);
        ini_set('display_errors', 0);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $columnas = array(
                'iddetalle'     => 'p' . date('Ymd-His', time()) . '-' . $this->metodos->generarCodigo(10),
                'idProducto'    => $_POST['idProducto'],
                'idPedido'      => $_POST['idPedido'],
                'cantidad'      => $_POST['cantidad'],
                'precio_actual' => str_replace(',', '.', ((string) $_POST['precio_actual'])),
                'f_alta'        => date('Y-m-d H:i:s', time()),
            );
            $filas = $this->ApiServiceModel->setDetallePedido($columnas);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Detalleenviadoexitosamente", $columnas);
            } else {
                $this->metodos->_resultClient(false, "Redinestable, noseenviosudetallevuelvaaintentarlomastarde . ", $filas);
                return;
            }
        }
    }
}
