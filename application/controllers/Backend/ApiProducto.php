<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('America/La_Paz'); #Línea agregada
class ApiProducto extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('ApiServiceModel');
        $this->load->library('Metodos');
    }

    public function index()
    {}

    public function getProductosEmpresa()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $filas = $this->ApiServiceModel->getProductosEmpresa($_POST['filtro'], $_POST['idEmpresa']);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Lista de productos: " . $_POST['filtro'], $filas);
            } else {
                $this->metodos->_resultClient(false, "No hay productos: " . $_POST['filtro'], $filas);
            }
        }
    }

    public function setProductoEmpresa()
    {
        //oculta las advertencias de codeingiter pra temp
        error_reporting(0);
        ini_set('display_errors', 0);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $columnas = array(
                'idProducto '    => 'p' . date('Ymd-His', time()) . '-' . $this->metodos->generarCodigo(10),
                'titulo'         => $_POST['titulo'],
                'descripcion'    => $_POST['descripcion'],
                'precio'         => str_replace(',', '.', ((string) $_POST['precio'])),
                'stock'          => $_POST['stock'],
                'foto'           => 'IMG-' . date('Ymd-His', time()) . '-' . $this->metodos->generarCodigo(10) . '.jpg',
                'f_alta'         => date('Y-m-d H:i:s', time()),
                'f_modificacion' => date('Y-m-d H:i:s', time()),
                'idEmpresa'      => $_POST['idEmpresa'],

            );
            //COPIAMOS LA FOTOGRAFIA DEL CLIENTE
            $file2             = $_FILES["foto"];
            $origen2           = $file2["tmp_name"];
            $destino2          = "public/folder/img_producto/" . $columnas['foto'];
            $destino_temporal2 = tempnam("tmp/", "tmp");

            if ($this->metodos->redimensionarImagen($origen2, $destino_temporal2, 240, 240, 100)) {
                $fp = fopen($destino2, "w");
                fputs($fp, fread(fopen($destino_temporal2, "r"), filesize($destino_temporal2)));
                fclose($fp);
            } else {
                //eliminamos las fotografias para no cargar al servidor
                $this->metodos->deleteFile("public/folder/img_producto/" . $columnas['foto']);
                $this->metodos->_resultClient(false, "Red inestable no se pudo guardar su fotografia, intentelo de nuevo.", []);
                return;
            }

            $filas = $this->ApiServiceModel->setProductoEmpresa($columnas);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Registro guardado exitosamente", $columnas);
            } else {
                $this->metodos->_resultClient(false, "Red inestable, no se guardo su registro vuelva a intentarlo mas tarde.", $filas);
                //eliminamos las fotografias para no cargar al servidor
                $this->metodos->deleteFile("public/folder/img_producto/" . $columnas['foto']);
                $this->metodos->_resultClient(false, "Red inestable no se pudo guardar su fotografia, intentelo de nuevo.", []);
                return;
            }
        }
    }

    public function cargaCiudades()
    {
        $filas = $this->ApiServiceModel->cargaCiudades();
        if (isset($filas)) {
            $this->metodos->_resultClient(true, "Datos cargados", $filas);
        } else {
            $this->metodos->_resultClient(false, "Sin datos cargados", []);
        }
    }

    public function cargaEncuentraMascotas()
    {
        $filas = $this->ApiServiceModel->cargaEncuentraMascotas($_GET['opcionCiudad']);
        if (isset($filas)) {
            $this->metodos->_resultClient(true, "Datos cargados", $filas);
        } else {
            $this->metodos->_resultClient(false, "Sin datos cargados", []);
        }
    }

    public function cargarFundaciones()
    {
        $filas = $this->ApiServiceModel->cargarFundaciones($_GET['opcionCiudad']);
        if (isset($filas)) {
            $this->metodos->_resultClient(true, "Datos cargados", $filas);
        } else {
            $this->metodos->_resultClient(false, "Sin datos cargados", []);
        }
    }

    public function cargarLocalizacion()
    {
        $filas = $this->ApiServiceModel->cargarLocalizacion();
        if (isset($filas)) {
            $this->metodos->_resultClient(true, "Datos cargados", $filas);
        } else {
            $this->metodos->_resultClient(false, "Sin datos cargados", []);
        }
    }

    public function cargarServicios()
    {
        $filas = $this->ApiServiceModel->cargarServicios($_GET['opcionCiudad']);
        if (isset($filas)) {
            $this->metodos->_resultClient(true, "Datos cargados", $filas);
        } else {
            $this->metodos->_resultClient(false, "Sin datos cargados", []);
        }
    }

    public function cargarVeterinarias()
    {
        $filas = $this->ApiServiceModel->cargarVeterinarias($_GET['opcionCiudad']);
        if (isset($filas)) {
            $this->metodos->_resultClient(true, "Datos cargados", $filas);
        } else {
            $this->metodos->_resultClient(false, "Sin datos cargados", []);
        }
    }

    public function datosAdoptaVida()
    {
        $filas = $this->ApiServiceModel->datosAdoptaVida($_GET['adoptaVida']);
        if (isset($filas)) {
            $this->metodos->_resultClient(true, "Datos cargados", $filas);
        } else {
            $this->metodos->_resultClient(false, "Sin datos cargados", []);
        }
    }

    public function datosEncuentraMascota()
    {
        $filas = $this->ApiServiceModel->datosEncuentraMascota($_GET['encuentraMascota']);
        if (isset($filas)) {
            $this->metodos->_resultClient(true, "Datos cargados", $filas);
        } else {
            $this->metodos->_resultClient(false, "Sin datos cargados", []);
        }
    }

    public function datosFundaciones()
    {
        $filas = $this->ApiServiceModel->datosFundaciones($_GET['fundaciones']);
        if (isset($filas)) {
            $this->metodos->_resultClient(true, "Datos cargados", $filas);
        } else {
            $this->metodos->_resultClient(false, "Sin datos cargados", []);
        }
    }

    public function datosVeterinario()
    {
        $filas = $this->ApiServiceModel->datosVeterinario($_GET['veterinario']);
        if (isset($filas)) {
            $this->metodos->_resultClient(true, "Datos cargados", $filas);
        } else {
            $this->metodos->_resultClient(false, "Sin datos cargados", []);
        }
    }

    public function detalleServicios()
    {
        $filas = $this->ApiServiceModel->detalleServicios($_GET['servicios']);
        if (isset($filas)) {
            $this->metodos->_resultClient(true, "Datos cargados", $filas);
        } else {
            $this->metodos->_resultClient(false, "Sin datos cargados", []);
        }
    }

    public function insertarMascota()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            //guardo la imagen
            $file_path = "public/encuentra_mascota/";
            $file_path = $file_path . basename($_FILES['imagen']['name']);
            if (move_uploaded_file($_FILES['imagen']['tmp_name'], $file_path)) {
                // echo "success";
            } else {
                $this->metodos->_resultClient(false, "El archivo no se pudo subir, intente de nuevo", []);
                return;
            }

            //inserta en la base de datos
            $columnas = array(
                'id_encuentra_mascota'          => null,
                'titulo_encuentra_mascota'      => $_POST['titulo'],
                'descripcion_encuentra_mascota' => $_POST['descripcion'],
                'imagen_encuentra_mascota'      => $_FILES['imagen']['name'],
                'correo_encuentra_mascota'      => $_POST['correo'],
                'telefono_encuentra_mascota'    => $_POST['telefono'],
                'estado'                        => 1,
            );
            $filas = $this->ApiServiceModel->setEncuentraMascota($columnas, $_POST['idciudad']);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Registro creadao exitosamente", $columnas);
            } else {
                $this->metodos->_resultClient(false, "Red inestable, no se registro.", $filas);
            }
        }
    }

    public function insertarServicio()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            //guardo la imagen
            $file_path = "public/servicios/";
            $file_path = $file_path . basename($_FILES['imagen']['name']);
            if (move_uploaded_file($_FILES['imagen']['tmp_name'], $file_path)) {
                // echo "success";
            } else {
                $this->metodos->_resultClient(false, "El archivo no se pudo subir, intente de nuevo", []);
                return;
            }

            //inserta en la base de datos
            $columnas = array(
                'id_servicio'          => null,
                'titulo_servicio'      => $_POST['titulo'],
                'descripcion_servicio' => $_POST['descripcion'],
                'imagen_servicio'      => $_FILES['imagen']['name'],
                'estado'               => 1,
            );
            $filas = $this->ApiServiceModel->setServicio($columnas);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Registro creadao exitosamente", $columnas);
            } else {
                $this->metodos->_resultClient(false, "Red inestable, no se registro.", $filas);
            }
        }
    }

    public function setVeterinario()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            //guardo la imagen
            $file_path = "public/veterinario/";
            $file_path = $file_path . basename($_FILES['imagen']['name']);
            if (move_uploaded_file($_FILES['imagen']['tmp_name'], $file_path)) {
                // echo "success";
            } else {
                $this->metodos->_resultClient(false, "El archivo no se pudo subir, intente de nuevo", []);
                return;
            }

            //inserta en la base de datos
            $columnas = array(
                'id_veterinario'          => null,
                'titulo_veterinario'      => $_POST['titulo'],
                'descripcion_veterinario' => $_POST['descripcion'],
                'imagen_veterinario'      => $_FILES['imagen']['name'],
                'correo_veterinario'      => $_POST['correo'],
                'telefono_veterinario'    => $_POST['telefono'],
                'estado'                  => 1,
            );
            $filas = $this->ApiServiceModel->setVeterinario($columnas);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Registro creadao exitosamente", $columnas);
            } else {
                $this->metodos->_resultClient(false, "Red inestable, no se registro.", $filas);
            }
        }
    }

    public function beta2()
    {
        $filas = $this->ApiServiceModel->getOfertasVista('La Paz', 'inicio', 'Lunes');
        if (isset($filas)) {
            $this->metodos->_resultSql_2($filas);
        }

        $temporada = '';

        $mes = date('m');
        $dia = date("d");

        if ($mes >= 1 && $mes <= 1 && $dia >= 1 && $dia <= 8) {
//AÑO NUEVO Y REYES MAGOS
            $temporada = "Regalo_Niño_Niña_Mujer_Hombre_Señorita_Esposa_Joven_Masitas";
        } else if ($mes >= 1 && $mes <= 1 && $dia >= 9 && $dia <= 22) {
//alasita
            $temporada = "Entretenimiento_Niño_Niña_Mujer_Hombre_Señorita_Esposa_Joven_Comida";
        } else if ($mes >= 2 && $mes <= 2 && $dia >= 1 && $dia <= 7) {
//carnavales
            $temporada = "Entretenimiento_Regalo_Niño_Niña_Fiesta";
        } else if ($mes >= 2 && $mes <= 2 && $dia >= 8 && $dia <= 33) {
//San valentin
            $temporada = "Entretenimiento_Regalo_Mujer_Hombre_Señorita_Esposa_Joven";
        } else if ($mes >= 3 && $mes <= 3 && $dia >= 1 && $dia <= 10) {
//Dia internacional de la Mujer 8 DE MARZO
            $temporada = "Madre_Mujer_Pareja_Señorita_Esposa";
        } else if ($mes >= 3 && $mes <= 3 && $dia >= 11 && $dia <= 20) {
//Dia del Padre
            $temporada = "Regalo_Pareja_Hombre_Padre_Joven";
        } else if ($mes >= 3 && $mes <= 3 && $dia >= 21 && $dia <= 33) {
//Dia del mar
            $temporada = "Comida_Entretenimiento_Educacion";
        } else if ($mes >= 4 && $mes <= 4 && $dia >= 1 && $dia <= 12) {
//Dia del Niño
            $temporada = "Comida_Entretenimiento_Regalo_Niño_Niña";
        } else if ($mes >= 4 && $mes <= 4 && $dia >= 12 && $dia <= 20) {
//Semana Santa
            $temporada = "Comida_Entretenimiento_Religion";
        } else if ($mes >= 4 && $mes <= 4 && $dia >= 21 && $dia <= 33) {
//Dia del trabajador
            $temporada = "Comida_Entretenimiento_Regalo_Mujer_Hombre_Esposo_Esposa";
        } else if ($mes >= 5 && $mes <= 5 && $dia >= 1 && $dia <= 2) {
//Dia del trabajador
            $temporada = "Comida_Entretenimiento_Regalo_Mujer_Hombre_Esposo_Esposa";
        } else if ($mes >= 5 && $mes <= 5 && $dia >= 3 && $dia <= 33) {
//Dia del la madre
            $temporada = "Comida_Entretenimiento_Regalo_Mujer_Esposa";
        } else if ($mes >= 6 && $mes <= 6 && $dia >= 1 && $dia <= 20) {
//Corpus cristi
            $temporada = "Comida_Entretenimiento";
        } else if ($mes >= 6 && $mes <= 6 && $dia >= 21 && $dia <= 25) {
//Año nuevo aymara ' san juan 21 Y 23
            $temporada = "Comida_Entretenimiento_Juego_Fiesta";
        } else if ($mes >= 7 && $mes <= 7 && $dia >= 1 && $dia <= 31) {
//Aniversario civico de la paz
            $temporada = "Comida_Entretenimiento_Juego_Vacaciones";
        } else if ($mes >= 8 && $mes <= 8 && $dia >= 1 && $dia <= 17) {
//Aniversario de bolivia fiestas patrias
            $temporada = "Comida_Entretenimiento_Juego_Vacaciones_Hombre_Mujer_Niño_Niña";
        } else if ($mes >= 9 && $mes <= 9 && $dia >= 1 && $dia <= 31) {
//Dia del estudiante, amor y primavera
            $temporada = "Comida_Entretenimiento_Juego_Vacaciones_Hombre_Mujer_Niño_Niña";
        } else if ($mes >= 10 && $mes <= 10 && $dia >= 1 && $dia <= 19) {
//Dia de la Mujer boliviana
            $temporada = "Madre_Mujer_Pareja_Señorita_Esposa";
        } else if ($mes >= 10 && $mes <= 10 && $dia >= 20 && $dia <= 33) {
//todos los santos
            $temporada = "Fiesta_Terror_Entretenimiento_Mujer_Hombre";
        } else if ($mes >= 11 && $mes <= 11 && $dia >= 1 && $dia <= 6) {
//todos los santos 2
            $temporada = "Fiesta_Terror_Entretenimiento_Mujer_Hombre";
        } else if ($mes >= 11 && $mes <= 11 && $dia >= 7 && $dia <= 33) {
//FINALEZ DE CLASES
            $temporada = "Educacion_Vacaciones_Entretenimiento_turismo_Mujer_Hombre";
        } else if ($mes >= 12 && $mes <= 12 && $dia >= 1 && $dia <= 33) {
//navidad
            $temporada = "Regalo_Niño_Niña_Mujer_Hombre_Señorita_Esposa_Joven_Masitas_Entretenimiento";
        } else {

        }
        echo "<br><br>Servicio buscador: <br>" . $temporada;
        $filas = $this->ApiServiceModel->getTemporadaVista($temporada, 'La Paz');
        $this->metodos->_resultSql_2($filas);
    }
    public function beta()
    {
        echo "<br><br>Servicio buscador: <br>";
        $filas = $this->ApiServiceModel->getBuscaServices($this->getTextoNormal('pollo'), 'Servicio', 2, 100, 'Todos', 'Todos');
        $this->metodos->_resultSql_2($filas);

        echo "<br><br>EMPRESAS buscador: <br>";
        $filas = $this->ApiServiceModel->getBuscaServices($this->getTextoNormal('pol'), 'Empresa', 2, 100, 'Todos', 'Todos');
        $this->metodos->_resultSql_2($filas);

        echo "<br><br>beta> " . $this->getTextoNormal("hola paluz[a]aá") . "<br><br>";
        $fila = $this->ApiServiceModel->getServices("categoria", "Turismo", "La Paz");
        // $fila = $this->ApiServiceModel->getUsuarioRepetido("pee@gmail.com");
        $this->metodos->_resultSql_2($fila);

        echo "<br><br>INICIO <br>";
        $filas = $this->ApiServiceModel->getServices("SALUD Y MEDICINA", "", "La Paz");
        if (isset($filas)) {
            $this->metodos->_resultSql_2($filas);
        }

        echo "<br><br>OFERTAS INICIO <br>";
        $filas = $this->ApiServiceModel->getOfertasIncio("La Paz");
        if (isset($filas)) {
            $this->metodos->_resultSql_2($filas);
        }
        // echo "<br><br>OFERTAS BUSQUEDA <br>";
        // $filas = $this->ApiServiceModel->getOfertasIncio();
        // if (isset($filas)) {
        //     $this->metodos->_resultSql_2($filas);
        // }

        echo "<br><br>PUBLICIDAD <br>";
        $filas = $this->ApiServiceModel->getPublicidad("La Paz");
        if (isset($filas)) {
            $this->metodos->_resultSql_2($filas);
        }

        // echo "Post <br><br>";
        // $filas = $this->ApiServiceModel->getPostTopVistos();
        // if (isset($filas)) {
        //     $this->metodos->_resultSql_2($filas);
        // }
        // $nick = 'quibo.developer@gmail.com';
        // if (isset($nick)) {
        //     $filas = $this->ApiServiceModel->getUsuario($nick);
        //     if (isset($filas)) {
        //         $this->metodos->_resultSql_2($filas);
        //     }
        // }
    }

    //USUSARIO Y LOGUEO
    public function usuario()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body = json_decode(file_get_contents("php://input"), true);
            $nick = $body['dato'];
            if (isset($nick)) {
                $filas = $this->ApiServiceModel->getUsuario($nick);
                if (isset($filas)) {
                    $this->metodos->_resultSql_2($filas);
                }
            }
        }
    }

    public function getVistaInicio()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body      = json_decode(file_get_contents("php://input"), true);
            $ciudad    = $body['ciudad'];
            $categoria = $body['categoria'];
            $total     = array();
            $filas     = $this->ApiServiceModel->getPublicidadPremiun($ciudad);
            if (isset($filas)) {
                foreach ($filas as $item) {
                    array_push($total, $item);
                }
                $filas2 = $this->ApiServiceModel->getPublicidadLite($ciudad);
                if (isset($filas2)) {
                    foreach ($filas2 as $item) {
                        array_push($total, $item);
                    }
                    //carga muro
                    $filas = $this->ApiServiceModel->getContenidoMuro($ciudad, $categoria);
                    if (isset($filas)) {
                        foreach ($filas as $item) {
                            array_push($total, $item);
                        }
                        if (count($filas) < 50) {
                            $filas = $this->ApiServiceModel->getContenidoMuroComplemento($ciudad, (50 - count($filas)));
                            foreach ($filas as $item) {
                                array_push($total, $item);
                            }
                            $this->metodos->_resultSql_2($total);
                        } else {
                            $this->metodos->_resultSql_2($total);
                        }
                    }
                }
            }
        }
    }

    public function getVistaSemanal() //tab

    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body   = json_decode(file_get_contents("php://input"), true);
            $ciudad = $body['ciudad'];
            $dia    = $body['dia'];
            $filas  = $this->ApiServiceModel->getVistaSemanal($ciudad, $dia);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getVistaOfertas()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body   = json_decode(file_get_contents("php://input"), true);
            $ciudad = $body['ciudad'];
            $filas  = $this->ApiServiceModel->getVistaOfertas($ciudad);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getVistaTemporada() //tab

    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body      = json_decode(file_get_contents("php://input"), true);
            $temporada = "";
            $ciudad    = $body['ciudad'];
            $mes       = date('m');
            $dia       = date("d");

            if ($mes >= 1 && $mes <= 1 && $dia >= 1 && $dia <= 8) {
//AÑO NUEVO Y REYES MAGOS
                $temporada = "Regalo_Niño_Niña_Mujer_Hombre_Señorita_Esposa_Joven_Masitas";
            } else if ($mes >= 1 && $mes <= 1 && $dia >= 9 && $dia <= 22) {
//alasita
                $temporada = "Entretenimiento_Niño_Niña_Mujer_Hombre_Señorita_Esposa_Joven_Comida";
            } else if ($mes >= 2 && $mes <= 2 && $dia >= 1 && $dia <= 7) {
//carnavales
                $temporada = "Entretenimiento_Regalo_Niño_Niña_Fiesta";
            } else if ($mes >= 2 && $mes <= 2 && $dia >= 8 && $dia <= 33) {
//San valentin
                $temporada = "Entretenimiento_Regalo_Mujer_Hombre_Señorita_Esposa_Joven";
            } else if ($mes >= 3 && $mes <= 3 && $dia >= 1 && $dia <= 10) {
//Dia internacional de la Mujer 8 DE MARZO
                $temporada = "Madre_Mujer_Pareja_Señorita_Esposa";
            } else if ($mes >= 3 && $mes <= 3 && $dia >= 11 && $dia <= 20) {
//Dia del Padre
                $temporada = "Regalo_Pareja_Hombre_Padre_Joven";
            } else if ($mes >= 3 && $mes <= 3 && $dia >= 21 && $dia <= 33) {
//Dia del mar
                $temporada = "Comida_Entretenimiento_Educacion";
            } else if ($mes >= 4 && $mes <= 4 && $dia >= 1 && $dia <= 12) {
//Dia del Niño
                $temporada = "Comida_Entretenimiento_Regalo_Niño_Niña";
            } else if ($mes >= 4 && $mes <= 4 && $dia >= 12 && $dia <= 20) {
//Semana Santa
                $temporada = "Comida_Entretenimiento_Religion";
            } else if ($mes >= 4 && $mes <= 4 && $dia >= 21 && $dia <= 33) {
//Dia del trabajador
                $temporada = "Comida_Entretenimiento_Regalo_Mujer_Hombre_Esposo_Esposa";
            } else if ($mes >= 5 && $mes <= 5 && $dia >= 1 && $dia <= 2) {
//Dia del trabajador
                $temporada = "Comida_Entretenimiento_Regalo_Mujer_Hombre_Esposo_Esposa";
            } else if ($mes >= 5 && $mes <= 5 && $dia >= 3 && $dia <= 33) {
//Dia del la madre
                $temporada = "Comida_Entretenimiento_Regalo_Mujer_Esposa";
            } else if ($mes >= 6 && $mes <= 6 && $dia >= 1 && $dia <= 20) {
//Corpus cristi
                $temporada = "Comida_Entretenimiento";
            } else if ($mes >= 6 && $mes <= 6 && $dia >= 21 && $dia <= 25) {
//Año nuevo aymara ' san juan 21 Y 23
                $temporada = "Comida_Entretenimiento_Juego_Fiesta";
            } else if ($mes >= 7 && $mes <= 7 && $dia >= 1 && $dia <= 31) {
//Aniversario civico de la paz
                $temporada = "Comida_Entretenimiento_Juego_Vacaciones";
            } else if ($mes >= 8 && $mes <= 8 && $dia >= 1 && $dia <= 17) {
//Aniversario de bolivia fiestas patrias
                $temporada = "Comida_Entretenimiento_Juego_Vacaciones_Hombre_Mujer_Niño_Niña";
            } else if ($mes >= 9 && $mes <= 9 && $dia >= 1 && $dia <= 31) {
//Dia del estudiante, amor y primavera
                $temporada = "Comida_Entretenimiento_Juego_Vacaciones_Hombre_Mujer_Niño_Niña";
            } else if ($mes >= 10 && $mes <= 10 && $dia >= 1 && $dia <= 19) {
//Dia de la Mujer boliviana
                $temporada = "Madre_Mujer_Pareja_Señorita_Esposa";
            } else if ($mes >= 10 && $mes <= 10 && $dia >= 20 && $dia <= 33) {
//todos los santos
                $temporada = "Fiesta_Terror_Entretenimiento_Mujer_Hombre";
            } else if ($mes >= 11 && $mes <= 11 && $dia >= 1 && $dia <= 6) {
//todos los santos 2
                $temporada = "Fiesta_Terror_Entretenimiento_Mujer_Hombre";
            } else if ($mes >= 11 && $mes <= 11 && $dia >= 7 && $dia <= 33) {
//FINALEZ DE CLASES
                $temporada = "Educacion_Vacaciones_Entretenimiento_turismo_Mujer_Hombre";
            } else if ($mes >= 12 && $mes <= 12 && $dia >= 1 && $dia <= 33) {
//navidad
                $temporada = "Regalo_Niño_Niña_Mujer_Hombre_Señorita_Esposa_Joven_Masitas_Entretenimiento";
            } else {

            }
            $filas = $this->ApiServiceModel->getVistaTemporada($temporada, $ciudad);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getVistaCategoria()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body      = json_decode(file_get_contents("php://input"), true);
            $ciudad    = $body['ciudad'];
            $categoria = $body['categoria'];
            $filas     = $this->ApiServiceModel->getVistaCategoria($ciudad, $categoria);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getBusquedaMuro()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body      = json_decode(file_get_contents("php://input"), true);
            $txtBuscar = $body['busqueda'];
            $ciudad    = $body['ciudad'];
            $filtro    = $body['filtro'];
            $filas     = $this->ApiServiceModel->getBusquedaMuro($txtBuscar, $filtro, $ciudad);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getServicioId()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body = json_decode(file_get_contents("php://input"), true);
            $id   = $body['id'];
            if (isset($id)) {
                $filas = $this->ApiServiceModel->getServicioId($id);
                if (isset($filas)) {
                    $this->metodos->_resultSql_2($filas);
                }
            }
        }
    }

//veta
    public function getServicio() //miniatura y vista

    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body   = json_decode(file_get_contents("php://input"), true);
            $tipo   = $body['tipo'];
            $busca  = $body['busca'];
            $ciudad = $body['ciudad'];
            if (isset($tipo)) {
                $filas = $this->ApiServiceModel->getServices($tipo, $busca, $ciudad);
                if (isset($filas)) {
                    $this->metodos->_resultSql_2($filas);
                }
            }
        }
    }

    public function getOfertasIncio() //miniatura

    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body   = json_decode(file_get_contents("php://input"), true);
            $ciudad = $body['ciudad'];
            $filas  = $this->ApiServiceModel->getOfertasIncio($ciudad);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getPublicidadInicio() //miniatura

    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body   = json_decode(file_get_contents("php://input"), true);
            $ciudad = $body['ciudad'];
            $filas  = $this->ApiServiceModel->getPublicidad($ciudad);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getOfertasVista() //tab

    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body         = json_decode(file_get_contents("php://input"), true);
            $ciudad       = $body['ciudad'];
            $categoria    = $body['categoria'];
            $tipoBusqueda = $body['tipoBusqueda'];
            $filas        = $this->ApiServiceModel->getOfertasVista($ciudad, $categoria, $tipoBusqueda);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getBuscaServices()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body      = json_decode(file_get_contents("php://input"), true);
            $buscar    = $this->getTextoNormal($body['buscar']);
            $tipo      = $body['tipo']; //Empresa, Servicio
            $edad1     = $body['edad1'];
            $edad2     = $body['edad2'];
            $modalidad = $body['modalidad']; //amigos, familia,etc
            $ciudad    = $body['ciudad'];

            $filas = $this->ApiServiceModel->getBuscaServices($buscar, $tipo, $edad1, $edad2, $modalidad, $ciudad);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }

            // $filas=$this->ApiServiceModel->getBuscaServices($this->getTextoNormal('coco'), 'Empresa', 2, 100, 'Todos', 'Todos');
        }
    }

    //SERVICIO DE VISITAS
    public function setVisita()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $muro  = $_POST["idservicio"];
            $filas = $this->ApiServiceModel->setVisita($muro);
            $this->metodos->_resultSql_2($filas);
        } else {
            $this->redireccionar('_muro/noAcceso');
        }
    }

//extras
    public function getTextoNormal($data)
    {
        $imagen_validad = str_replace(array('*', '&', '^', '%', '$', '#', '@', '!', '(', ')', '_', '+', '-', '=', '{', '[', ']', '}', '|', "'", ';', '"', ':', ',', '<', '.', '>', '/', '?', '¿', '¡', '~', '`', '°', 'ñ', 'Ñ', 'á', 'í', 'é', 'ú', 'ó', 'Á', 'É', 'Í', 'Ó', 'Ú'), '', $data);
        // return str_replace(' ', '-', $imagen_validad);
        return $imagen_validad;
    }

    public function updatePostVisitas()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body = json_decode(file_get_contents("php://input"), true);
            // $columnas = array(
            //     'idpost'  => $_POST['idpost'],
            //     'visitas' => 'visitas + 1',
            // );
            $filas = $this->ApiServiceModel->updatePostVisita($_POST['idpost']);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }
    //HOROSCOPO
    public function getHoroscopo()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body  = json_decode(file_get_contents("php://input"), true);
            $filas = $this->ApiServiceModel->getHoroscopo();
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    // INSERTA EN BASE DE DATOS/
    public function setLoginSuscriptor()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body   = json_decode(file_get_contents("php://input"), true);
            $sesion = $body['sesion'];
            switch ($sesion) {
                case 'GOOGLE':
                    $email       = $body['correo'];
                    $filaUsuario = $this->ApiServiceModel->getUsuarioRepetido($email);
                    if ($filaUsuario['idsuscriptor'] == null) {
                        $columnas = array(
                            'idsuscriptor' => 'cl_' . date('Ymd_His') . '_' . $this->generarCodigo(5),
                            'nick'         => $body['nick'],
                            'correo'       => $email,
                            'password'     => 'GOOGLE',
                            'top1'         => 0,
                            'top2'         => 0,
                            'logros'       => 'Sabiondo supremo-0_Arriesgado-0_Estudiante-0_Maestro-0_Corcho-0_Desafiante-0_Invicto-0_Glorioso-0_Combatiente de Ciencia-0_Demoledor Historico-0_Sabio del Entretenimiento-0_Erudito del Arte-0_Maestro Geográfico-0',
                            'monedas'      => 40,
                            'f_nacimiento' => '2019-01-01',
                            'f_alta'       => date('Y-m-d'),
                            'f_baja'       => date('Y-m-d'),
                            'estado'       => 'A',
                            'tipo'         => 'Suscriptor',
                            'foto'         => $body['foto'],
                            'premio'       => '0',
                        );
                        $fila = $this->ApiServiceModel->setUsuario($columnas);
                        if (isset($fila)) {
                            $this->metodos->_resultSql_2($columnas);
                        } else {
                            echo json_encode([
                                "Message" => "error",
                                "Status"  => "bad",
                            ]);
                        }
                    } else {
                        $this->metodos->_resultSql_2($filaUsuario);
                    }
                    break;

                case 'SABIONDO':
                    $email = $body['correo'];
                    $fila  = $this->ApiServiceModel->getUsuarioRepetido($email);
                    $this->metodos->_resultSql_2($fila);

                    break;
            }
        }
    }

    public function setSuscriptor()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // $body     = json_decode(file_get_contents("php://input"), true);
            $columnas = array(
                'idsuscriptor' => 'cl_' . date('Ymd_His') . '_' . $this->generarCodigo(5),
                'nick'         => $_POST['nick'],
                'correo'       => $_POST['correo'],
                'password'     => $_POST['password'],
                'top1'         => 0,
                'top2'         => 0,
                'logros'       => 'Sabiondo supremo-0_Arriesgado-0_Estudiante-0_Maestro-0_Corcho-0_Desafiante-0_Invicto-0_Glorioso-0_Combatiente de Ciencia-0_Demoledor Historico-0_Sabio del Entretenimiento-0_Erudito del Arte-0_Maestro Geográfico-0',
                'monedas'      => 40,
                'f_nacimiento' => '1992-01-01',
                'f_alta'       => date('Y-m-d'),
                'f_baja'       => date('Y-m-d'),
                'estado'       => 'A',
                'tipo'         => 'Suscriptor',
                'foto'         => 'IMG_' . date('Ymd_His') . '_' . $this->generarCodigo(5) . ".jpeg",
                'premio'       => '0',
            );

            $fila = $this->ApiServiceModel->setUsuario($columnas);
            if (isset($fila)) {
                echo json_encode([
                    "Message" => "OK",
                    "Status"  => "good",
                ]);
                $image       = $_POST["foto"];
                $upload_path = "public/folder/img_cliente/" . $columnas['foto'];
                file_put_contents($upload_path, base64_decode($image));

                $image       = $_POST["foto_lite"];
                $upload_path = "public/folder/img_cliente/lite/" . $columnas['foto'];
                file_put_contents($upload_path, base64_decode($image));
            } else {
                echo json_encode([
                    "Message" => "error",
                    "Status"  => "bad",
                ]);
            }
        }
    }

    //ACTUALIZA MONEDAS Y TROFEOS SUSCRIPTOR
    public function updateMonedaTrofeo()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // $body     = json_decode(file_get_contents("php://input"), true);
            $columnas = array(
                'idsuscriptor' => $_POST['id'],
                'top1'         => $_POST['top1'],
                'logros'       => $_POST['logros'],
                'monedas'      => $_POST['monedas'],
            );

            $fila = $this->ApiServiceModel->setUpdateUsuario($columnas);
            if (isset($fila)) {
                echo json_encode([
                    "Message" => "OK",
                    "Status"  => "good",
                ]);
            } else {
                echo json_encode([
                    "Message" => "error",
                    "Status"  => "bad",
                ]);
            }
        }
    }
    public function updateMonedaPremio()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // $body     = json_decode(file_get_contents("php://input"), true);
            $columnas = array(
                'idsuscriptor' => $_POST['id'],
                'monedas'      => $_POST['monedas'],
                'premio'       => "0",
            );

            $fila = $this->ApiServiceModel->setUpdateUsuario($columnas);
            if (isset($fila)) {
                echo json_encode([
                    "Message" => "OK",
                    "Status"  => "good",
                ]);
            } else {
                echo json_encode([
                    "Message" => "error",
                    "Status"  => "bad",
                ]);
            }
        }
    }

    public function getTrofeos()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body  = json_decode(file_get_contents("php://input"), true);
            $filas = $this->ApiServiceModel->getTrofeos($body['id']);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    //TORNEOS
    public function getTorneo()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body  = json_decode(file_get_contents("php://input"), true);
            $filas = $this->ApiServiceModel->getTorneo();
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getClasificacionTorneo()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body  = json_decode(file_get_contents("php://input"), true);
            $filas = $this->ApiServiceModel->getClasificacionTorneo();
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function closedTorneo($value)
    {
        if ($value == 4991) {
            // sacar a los 3 top globales
            $filas_tops = $this->ApiServiceModel->getTopsTorneo();
            for ($i = 0; $i < count($filas_tops); $i++) {
                //actualizando el premio de los jugadores seleccionados
                switch ($i) {
                    case 0:
                        $columnas_tops = array(
                            'idsuscriptor' => $filas_tops[$i]->idsuscriptor,
                            'premio'       => '1er-600',
                        );
                        break;
                    case 1:
                        $columnas_tops = array(
                            'idsuscriptor' => $filas_tops[$i]->idsuscriptor,
                            'premio'       => '2do-500',
                        );
                        break;
                    case 2:
                        $columnas_tops = array(
                            'idsuscriptor' => $filas_tops[$i]->idsuscriptor,
                            'premio'       => '3er-450',
                        );
                        break;
                }
                $this->ApiServiceModel->setUpdateUsuario($columnas_tops);
            }
            //acualizo los trofeos actuales
            $filas_cambio = $this->ApiServiceModel->updateTrofeosCambio();
            if ($filas_cambio) {
                echo "ok";
            } else {
                echo "error";
            }
        }
    }

    //estra
    public function generarCodigo($longitud)
    {
        $key     = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max     = strlen($pattern) - 1;
        for ($i = 0; $i < $longitud; $i++) {
            $key .= $pattern{mt_rand(0, $max)};
        }
        return $key;
    }

    //prueba
    public function getUsuario()
    {
        // $arrayName = array('user' => $_POST['email'],'password' => $_POST['password'],'tipo' => 'Administrador', );
        $arrayName = array('correo' => 'gmail@', 'password' => 'locopass', 'tipo' => 'Administrador');
        print_r(json_encode($arrayName));
    }
}
