<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('America/La_Paz'); #Línea agregada
class ApiCliente extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('ApiServiceModel');
        $this->load->library('Metodos');
    }

    public function index()
    {
        //SERVICIO PARA DETECTAR ACTUALIZACIONES O MIGRACIONES, DONDE LA API SOLO DEBE PODER CONSULTAR Y NO AGREGAR, NI EDITAR, NI ELIMINAR
        //servicio para dectectar dispostitivos que utilizan una dicha version o mandar actuaizacion forzoza desde la app
    }

    //USUSARIO Y LOGUEO
    public function getLoginCliente()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $filas = $this->ApiServiceModel->getLoginCliente($_POST['numero']);
            if (isset($filas)) {
                if ($filas['estado'] != 'Restringido') {
                    $this->metodos->_resultClient(true, "Usuario activo", $filas);
                } else {
                    $this->metodos->_resultClient(false, "Usuario restrinjido, contactese con el administrador", array('estado' => 'Restringido'));
                }
            } else {
                $this->metodos->_resultClient(false, "El usuario no esta registrado", array('estado' => 'Nuevo'));
            }
        }
    }

    public function setCliente()
    {
        //oculta las advertencias de codeingiter pra temp
        error_reporting(0);
        ini_set('display_errors', 0);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($this->ApiServiceModel->getExistClienteCelular($_POST['numero'])) {
                $this->metodos->_resultClient(false, "El numero ya esta registrado", []);
                return;
            }
            $columnas = array(
                'idCliente'      => 'cl' . date('Ymd-His', time()) . '-' . $this->metodos->generarCodigo(10),
                'nombres'        => $_POST['nombres'],
                'apellidos'      => $_POST['apellidos'],
                'numero'         => $_POST['numero'],
                'ci'             => $_POST['ci'],
                'foto'           => 'IMG-' . date('Ymd-His', time()) . '-' . $this->metodos->generarCodigo(10) . '.jpg',
                'estado'         => 'Activo',
                'f_alta'         => date('Y-m-d H:i:s', time()),
                'f_modificacion' => date('Y-m-d H:i:s', time()),
            );
            //COPIAMOS LA FOTOGRAFIA DEL CLIENTE
            $file2             = $_FILES["foto"];
            $origen2           = $file2["tmp_name"];
            $destino2          = "public/folder/img_cliente_lite/" . $columnas['foto'];
            $destino_temporal2 = tempnam("tmp/", "tmp");

            if ($this->metodos->redimensionarImagen($origen2, $destino_temporal2, 62, 62, 100)) {
                $fp = fopen($destino2, "w");
                fputs($fp, fread(fopen($destino_temporal2, "r"), filesize($destino_temporal2)));
                fclose($fp);
                //copea la imagen original
                $file2             = $_FILES["foto"];
                $origen2           = $file2["tmp_name"];
                $destino_temporal2 = tempnam("tmp/", "tmp");
                $destino2          = "public/folder/img_cliente/" . $columnas['foto'];
                if ($this->metodos->redimensionarImagen($origen2, $destino_temporal2, 640, 640, 60)) {
                    $fp = fopen($destino2, "w");
                    fputs($fp, fread(fopen($destino_temporal2, "r"), filesize($destino_temporal2)));
                    fclose($fp);
                } else {
                    //eliminamos las fotografias para no cargar al servidor
                    $this->metodos->deleteFile("public/folder/img_cliente_lite/" . $columnas['foto']);
                    $this->metodos->deleteFile("public/folder/img_cliente/" . $columnas['foto']);
                    $this->metodos->_resultClient(false, "Red inestable no se pudo guardar su fotografia, intentelo de nuevo.", []);
                    return;
                }
            } else {
                //eliminamos las fotografias para no cargar al servidor
                $this->metodos->deleteFile("public/folder/img_cliente_lite/" . $columnas['foto']);
                $this->metodos->deleteFile("public/folder/img_cliente/" . $columnas['foto']);
                $this->metodos->_resultClient(false, "Red inestable no se pudo guardar su fotografia, intentelo de nuevo.", []);
                return;
            }

            $filas = $this->ApiServiceModel->setCliente($columnas);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Cuenta creada exitosamente", $columnas);
            } else {
                $this->metodos->_resultClient(false, "Red inestable, no se regsitro su cuenta vuelva a intentarlo mas tarde.", $filas);
                //eliminamos las fotografias para no cargar al servidor
                $this->metodos->deleteFile("public/folder/img_cliente_lite/" . $columnas['foto']);
                $this->metodos->deleteFile("public/folder/img_cliente/" . $columnas['foto']);
                $this->metodos->_resultClient(false, "Red inestable no se pudo guardar su fotografia, intentelo de nuevo.", []);
                return;
            }
        }
    }

    public function getPublicidadMuroInicio()
    {
        // $combined = array_merge($arr1, $arr2);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            //SACO LAS 5 PUBLICACIONES RANDOM DE PUBLICIDADES PAGAS por ciudad
            $filas          = $this->ApiServiceModel2->getPublicacionesPagaCiudad($_POST['ciudad'], 10, "PUBLICIDAD");
            $_PUBLICACIONES = $filas;
            if (count($filas) < 10) {
                //SACO LAS 5 PUBLICACIONES RANDOM DE PUBLICIDADES PAGAS EN GENERAL
                $filas          = $this->ApiServiceModel2->getPublicacionesPaga(10 - count($filas), "PUBLICIDAD");
                $_PUBLICACIONES = array_merge($_PUBLICACIONES, $filas);
                //SI NO HAY PUBLICIDADES ENTONCES SACAMOS PUBLICACIONES NORMALES POR CIUDAD
                if (count($filas) < 10) {
                    //SI NO HAY PUBLICIDADES ENTONCES SACAMOS PUBLICACIONES NORMALES POR CIUDAD
                    $filas          = $this->ApiServiceModel2->getPublicacionesCiudad($_POST['ciudad'], 10 - count($filas), "PUBLICIDAD");
                    $_PUBLICACIONES = array_merge($_PUBLICACIONES, $filas);
                    if (count($filas) < 10) {
                        //SACO LAS 5 PUBLICACIONES RANDOM DE PUBLICIDADES NORMALES EN GENERAL
                        $filas          = $this->ApiServiceModel2->getPublicacionesGenerales(10 - count($filas), "PUBLICIDAD");
                        $_PUBLICACIONES = array_merge($_PUBLICACIONES, $filas);
                    }
                }
            }

            //SACAMOS PUBLICACIONES NORMALES respetando ciudad y categorias
            $filas          = $this->ApiServiceModel2->getPublicacionesCiudadCategoria($_POST['ciudad'], $_POST['categoria'], 50, "NORMAL");
            $_PUBLICACIONES = array_merge($_PUBLICACIONES, $filas);
            if (count($filas) < 50) {
                //SACAMOS PUBLICACIONES NORMALES POR CIUDAD
                $filas          = $this->ApiServiceModel2->getPublicacionesGeneralCategoria($_POST['categoria'], 50 - count($filas), "NORMAL");
                $_PUBLICACIONES = array_merge($_PUBLICACIONES, $filas);
                if (count($filas) < 50) {
                    //SACO LAS 50 PUBLICACIONES RANDOM DE PUBLICIDADES NORMALES POR CATEGORIAS
                    $filas          = $this->ApiServiceModel2->getPublicacionesCiudad($_POST['ciudad'], 50, "NORMAL");
                    $_PUBLICACIONES = array_merge($_PUBLICACIONES, $filas);
                    if (count($filas) < 50) {
                        //SACO LAS 50 PUBLICACIONES RANDOM DE PUBLICIDADES NORMALES EN GENERAL
                        $filas          = $this->ApiServiceModel2->getPublicacionesGenerales(50 - count($filas), "NORMAL");
                        $_PUBLICACIONES = array_merge($_PUBLICACIONES, $filas);
                    }
                }
            }

            if (count($_PUBLICACIONES) > 0) {
                $this->metodos->_resultClient(true, "Lista de publicaciones para el muro de inicio", $_PUBLICACIONES);
            } else {
                $this->metodos->_resultClient(false, "No hay publicaciones recientes", []);
            }
        }
    }

    public function getPublicacionesSemanal()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $filas = $this->ApiServiceModel2->getPublicacionesSemanal($_POST['ciudad'], $_POST['categoria'], $_POST['semana'], 50, 'NORMAL');
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Lista de publicaciones semanales", $filas);
            } else {
                $this->metodos->_resultClient(false, "No hay publicaciones recientes", $filas);
            }
        }
    }

    public function getPublicacionesOferta()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $filas = $this->ApiServiceModel2->getPublicacionesOferta($_POST['ciudad'], $_POST['categoria'], 50, 'NORMAL');
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Lista de publicaciones de oferta", $filas);
            } else {
                $this->metodos->_resultClient(false, "No hay publicaciones recientes", $filas);
            }
        }
    }

    public function getPublicaionesTemporada() //tab

    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $temporada = "";
            $mes       = date('m');
            $dia       = date("d");

            if ($mes >= 1 && $mes <= 1 && $dia >= 1 && $dia <= 8) {
//AÑO NUEVO Y REYES MAGOS
                $temporada = "Regalo_Niño_Niña_Mujer_Hombre_Señorita_Esposa_Joven_Masitas";
            } else if ($mes >= 1 && $mes <= 1 && $dia >= 9 && $dia <= 22) {
//alasita
                $temporada = "Entretenimiento_Niño_Niña_Mujer_Hombre_Señorita_Esposa_Joven_Comida";
            } else if ($mes >= 2 && $mes <= 2 && $dia >= 1 && $dia <= 7) {
//carnavales
                $temporada = "Entretenimiento_Regalo_Niño_Niña_Fiesta";
            } else if ($mes >= 2 && $mes <= 2 && $dia >= 8 && $dia <= 33) {
//San valentin
                $temporada = "Entretenimiento_Regalo_Mujer_Hombre_Señorita_Esposa_Joven";
            } else if ($mes >= 3 && $mes <= 3 && $dia >= 1 && $dia <= 10) {
//Dia internacional de la Mujer 8 DE MARZO
                $temporada = "Madre_Mujer_Pareja_Señorita_Esposa";
            } else if ($mes >= 3 && $mes <= 3 && $dia >= 11 && $dia <= 20) {
//Dia del Padre
                $temporada = "Regalo_Pareja_Hombre_Padre_Joven";
            } else if ($mes >= 3 && $mes <= 3 && $dia >= 21 && $dia <= 33) {
//Dia del mar
                $temporada = "Comida_Entretenimiento_Educacion";
            } else if ($mes >= 4 && $mes <= 4 && $dia >= 1 && $dia <= 12) {
//Dia del Niño
                $temporada = "Comida_Entretenimiento_Regalo_Niño_Niña";
            } else if ($mes >= 4 && $mes <= 4 && $dia >= 12 && $dia <= 20) {
//Semana Santa
                $temporada = "Comida_Entretenimiento_Religion";
            } else if ($mes >= 4 && $mes <= 4 && $dia >= 21 && $dia <= 33) {
//Dia del trabajador
                $temporada = "Comida_Entretenimiento_Regalo_Mujer_Hombre_Esposo_Esposa";
            } else if ($mes >= 5 && $mes <= 5 && $dia >= 1 && $dia <= 2) {
//Dia del trabajador
                $temporada = "Comida_Entretenimiento_Regalo_Mujer_Hombre_Esposo_Esposa";
            } else if ($mes >= 5 && $mes <= 5 && $dia >= 3 && $dia <= 33) {
//Dia del la madre
                $temporada = "Comida_Entretenimiento_Regalo_Mujer_Esposa";
            } else if ($mes >= 6 && $mes <= 6 && $dia >= 1 && $dia <= 20) {
//Corpus cristi
                $temporada = "Comida_Entretenimiento";
            } else if ($mes >= 6 && $mes <= 6 && $dia >= 21 && $dia <= 25) {
//Año nuevo aymara ' san juan 21 Y 23
                $temporada = "Comida_Entretenimiento_Juego_Fiesta";
            } else if ($mes >= 7 && $mes <= 7 && $dia >= 1 && $dia <= 31) {
//Aniversario civico de la paz
                $temporada = "Comida_Entretenimiento_Juego_Vacaciones";
            } else if ($mes >= 8 && $mes <= 8 && $dia >= 1 && $dia <= 17) {
//Aniversario de bolivia fiestas patrias
                $temporada = "Comida_Entretenimiento_Juego_Vacaciones_Hombre_Mujer_Niño_Niña";
            } else if ($mes >= 9 && $mes <= 9 && $dia >= 1 && $dia <= 31) {
//Dia del estudiante, amor y primavera
                $temporada = "Comida_Entretenimiento_Juego_Vacaciones_Hombre_Mujer_Niño_Niña";
            } else if ($mes >= 10 && $mes <= 10 && $dia >= 1 && $dia <= 19) {
//Dia de la Mujer boliviana
                $temporada = "Madre_Mujer_Pareja_Señorita_Esposa";
            } else if ($mes >= 10 && $mes <= 10 && $dia >= 20 && $dia <= 33) {
//todos los santos
                $temporada = "Fiesta_Terror_Entretenimiento_Mujer_Hombre";
            } else if ($mes >= 11 && $mes <= 11 && $dia >= 1 && $dia <= 6) {
//todos los santos 2
                $temporada = "Fiesta_Terror_Entretenimiento_Mujer_Hombre";
            } else if ($mes >= 11 && $mes <= 11 && $dia >= 7 && $dia <= 33) {
//FINALEZ DE CLASES
                $temporada = "Educacion_Vacaciones_Entretenimiento_turismo_Mujer_Hombre";
            } else if ($mes >= 12 && $mes <= 12 && $dia >= 1 && $dia <= 33) {
//navidad
                $temporada = "Regalo_Niño_Niña_Mujer_Hombre_Señorita_Esposa_Joven_Masitas_Entretenimiento";
            } else {

            }
            $filas = $this->ApiServiceModel2->getVistaTemporada($temporada, $_POST['ciudad'], 50, 'NORMAL');
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Lista de publicaciones de temporada", $filas);
            } else {
                $this->metodos->_resultClient(false, "No hay publicaciones recientes", $filas);
            }
        }
    }

    public function getVistaInicio()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body      = json_decode(file_get_contents("php://input"), true);
            $ciudad    = $body['ciudad'];
            $categoria = $body['categoria'];
            $total     = array();
            $filas     = $this->ApiServiceModel->getPublicidadPremiun($ciudad);
            if (isset($filas)) {
                foreach ($filas as $item) {
                    array_push($total, $item);
                }
                $filas2 = $this->ApiServiceModel->getPublicidadLite($ciudad);
                if (isset($filas2)) {
                    foreach ($filas2 as $item) {
                        array_push($total, $item);
                    }
                    //carga muro
                    $filas = $this->ApiServiceModel->getContenidoMuro($ciudad, $categoria);
                    if (isset($filas)) {
                        foreach ($filas as $item) {
                            array_push($total, $item);
                        }
                        if (count($filas) < 50) {
                            $filas = $this->ApiServiceModel->getContenidoMuroComplemento($ciudad, (50 - count($filas)));
                            foreach ($filas as $item) {
                                array_push($total, $item);
                            }
                            $this->metodos->_resultSql_2($total);
                        } else {
                            $this->metodos->_resultSql_2($total);
                        }
                    }
                }
            }
        }
    }

    public function getVistaSemanal() //tab

    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body   = json_decode(file_get_contents("php://input"), true);
            $ciudad = $body['ciudad'];
            $dia    = $body['dia'];
            $filas  = $this->ApiServiceModel->getVistaSemanal($ciudad, $dia);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getVistaOfertas()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body   = json_decode(file_get_contents("php://input"), true);
            $ciudad = $body['ciudad'];
            $filas  = $this->ApiServiceModel->getVistaOfertas($ciudad);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getVistaTemporada() //tab

    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body      = json_decode(file_get_contents("php://input"), true);
            $temporada = "";
            $ciudad    = $body['ciudad'];
            $mes       = date('m');
            $dia       = date("d");

            if ($mes >= 1 && $mes <= 1 && $dia >= 1 && $dia <= 8) {
//AÑO NUEVO Y REYES MAGOS
                $temporada = "Regalo_Niño_Niña_Mujer_Hombre_Señorita_Esposa_Joven_Masitas";
            } else if ($mes >= 1 && $mes <= 1 && $dia >= 9 && $dia <= 22) {
//alasita
                $temporada = "Entretenimiento_Niño_Niña_Mujer_Hombre_Señorita_Esposa_Joven_Comida";
            } else if ($mes >= 2 && $mes <= 2 && $dia >= 1 && $dia <= 7) {
//carnavales
                $temporada = "Entretenimiento_Regalo_Niño_Niña_Fiesta";
            } else if ($mes >= 2 && $mes <= 2 && $dia >= 8 && $dia <= 33) {
//San valentin
                $temporada = "Entretenimiento_Regalo_Mujer_Hombre_Señorita_Esposa_Joven";
            } else if ($mes >= 3 && $mes <= 3 && $dia >= 1 && $dia <= 10) {
//Dia internacional de la Mujer 8 DE MARZO
                $temporada = "Madre_Mujer_Pareja_Señorita_Esposa";
            } else if ($mes >= 3 && $mes <= 3 && $dia >= 11 && $dia <= 20) {
//Dia del Padre
                $temporada = "Regalo_Pareja_Hombre_Padre_Joven";
            } else if ($mes >= 3 && $mes <= 3 && $dia >= 21 && $dia <= 33) {
//Dia del mar
                $temporada = "Comida_Entretenimiento_Educacion";
            } else if ($mes >= 4 && $mes <= 4 && $dia >= 1 && $dia <= 12) {
//Dia del Niño
                $temporada = "Comida_Entretenimiento_Regalo_Niño_Niña";
            } else if ($mes >= 4 && $mes <= 4 && $dia >= 12 && $dia <= 20) {
//Semana Santa
                $temporada = "Comida_Entretenimiento_Religion";
            } else if ($mes >= 4 && $mes <= 4 && $dia >= 21 && $dia <= 33) {
//Dia del trabajador
                $temporada = "Comida_Entretenimiento_Regalo_Mujer_Hombre_Esposo_Esposa";
            } else if ($mes >= 5 && $mes <= 5 && $dia >= 1 && $dia <= 2) {
//Dia del trabajador
                $temporada = "Comida_Entretenimiento_Regalo_Mujer_Hombre_Esposo_Esposa";
            } else if ($mes >= 5 && $mes <= 5 && $dia >= 3 && $dia <= 33) {
//Dia del la madre
                $temporada = "Comida_Entretenimiento_Regalo_Mujer_Esposa";
            } else if ($mes >= 6 && $mes <= 6 && $dia >= 1 && $dia <= 20) {
//Corpus cristi
                $temporada = "Comida_Entretenimiento";
            } else if ($mes >= 6 && $mes <= 6 && $dia >= 21 && $dia <= 25) {
//Año nuevo aymara ' san juan 21 Y 23
                $temporada = "Comida_Entretenimiento_Juego_Fiesta";
            } else if ($mes >= 7 && $mes <= 7 && $dia >= 1 && $dia <= 31) {
//Aniversario civico de la paz
                $temporada = "Comida_Entretenimiento_Juego_Vacaciones";
            } else if ($mes >= 8 && $mes <= 8 && $dia >= 1 && $dia <= 17) {
//Aniversario de bolivia fiestas patrias
                $temporada = "Comida_Entretenimiento_Juego_Vacaciones_Hombre_Mujer_Niño_Niña";
            } else if ($mes >= 9 && $mes <= 9 && $dia >= 1 && $dia <= 31) {
//Dia del estudiante, amor y primavera
                $temporada = "Comida_Entretenimiento_Juego_Vacaciones_Hombre_Mujer_Niño_Niña";
            } else if ($mes >= 10 && $mes <= 10 && $dia >= 1 && $dia <= 19) {
//Dia de la Mujer boliviana
                $temporada = "Madre_Mujer_Pareja_Señorita_Esposa";
            } else if ($mes >= 10 && $mes <= 10 && $dia >= 20 && $dia <= 33) {
//todos los santos
                $temporada = "Fiesta_Terror_Entretenimiento_Mujer_Hombre";
            } else if ($mes >= 11 && $mes <= 11 && $dia >= 1 && $dia <= 6) {
//todos los santos 2
                $temporada = "Fiesta_Terror_Entretenimiento_Mujer_Hombre";
            } else if ($mes >= 11 && $mes <= 11 && $dia >= 7 && $dia <= 33) {
//FINALEZ DE CLASES
                $temporada = "Educacion_Vacaciones_Entretenimiento_turismo_Mujer_Hombre";
            } else if ($mes >= 12 && $mes <= 12 && $dia >= 1 && $dia <= 33) {
//navidad
                $temporada = "Regalo_Niño_Niña_Mujer_Hombre_Señorita_Esposa_Joven_Masitas_Entretenimiento";
            } else {

            }
            $filas = $this->ApiServiceModel->getVistaTemporada($temporada, $ciudad);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getVistaCategoria()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body      = json_decode(file_get_contents("php://input"), true);
            $ciudad    = $body['ciudad'];
            $categoria = $body['categoria'];
            $filas     = $this->ApiServiceModel->getVistaCategoria($ciudad, $categoria);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getBusquedaMuro()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body      = json_decode(file_get_contents("php://input"), true);
            $txtBuscar = $body['busqueda'];
            $ciudad    = $body['ciudad'];
            $filtro    = $body['filtro'];
            $filas     = $this->ApiServiceModel->getBusquedaMuro($txtBuscar, $filtro, $ciudad);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getServicioId()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body = json_decode(file_get_contents("php://input"), true);
            $id   = $body['id'];
            if (isset($id)) {
                $filas = $this->ApiServiceModel->getServicioId($id);
                if (isset($filas)) {
                    $this->metodos->_resultSql_2($filas);
                }
            }
        }
    }

//veta
    public function getServicio() //miniatura y vista

    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body   = json_decode(file_get_contents("php://input"), true);
            $tipo   = $body['tipo'];
            $busca  = $body['busca'];
            $ciudad = $body['ciudad'];
            if (isset($tipo)) {
                $filas = $this->ApiServiceModel->getServices($tipo, $busca, $ciudad);
                if (isset($filas)) {
                    $this->metodos->_resultSql_2($filas);
                }
            }
        }
    }

    public function getOfertasIncio() //miniatura

    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body   = json_decode(file_get_contents("php://input"), true);
            $ciudad = $body['ciudad'];
            $filas  = $this->ApiServiceModel->getOfertasIncio($ciudad);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getPublicidadInicio() //miniatura

    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body   = json_decode(file_get_contents("php://input"), true);
            $ciudad = $body['ciudad'];
            $filas  = $this->ApiServiceModel->getPublicidad($ciudad);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getOfertasVista() //tab

    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body         = json_decode(file_get_contents("php://input"), true);
            $ciudad       = $body['ciudad'];
            $categoria    = $body['categoria'];
            $tipoBusqueda = $body['tipoBusqueda'];
            $filas        = $this->ApiServiceModel->getOfertasVista($ciudad, $categoria, $tipoBusqueda);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    public function getBuscaServices()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body      = json_decode(file_get_contents("php://input"), true);
            $buscar    = $this->getTextoNormal($body['buscar']);
            $tipo      = $body['tipo']; //Empresa, Servicio
            $edad1     = $body['edad1'];
            $edad2     = $body['edad2'];
            $modalidad = $body['modalidad']; //amigos, familia,etc
            $ciudad    = $body['ciudad'];

            $filas = $this->ApiServiceModel->getBuscaServices($buscar, $tipo, $edad1, $edad2, $modalidad, $ciudad);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }

            // $filas=$this->ApiServiceModel->getBuscaServices($this->getTextoNormal('coco'), 'Empresa', 2, 100, 'Todos', 'Todos');
        }
    }

    //SERVICIO DE VISITAS
    public function setVisita()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $muro  = $_POST["idservicio"];
            $filas = $this->ApiServiceModel->setVisita($muro);
            $this->metodos->_resultSql_2($filas);
        } else {
            $this->redireccionar('_muro/noAcceso');
        }
    }

//extras
    public function getTextoNormal($data)
    {
        $imagen_validad = str_replace(array('*', '&', '^', '%', '$', '#', '@', '!', '(', ')', '_', '+', '-', '=', '{', '[', ']', '}', '|', "'", ';', '"', ':', ',', '<', '.', '>', '/', '?', '¿', '¡', '~', '`', '°', 'ñ', 'Ñ', 'á', 'í', 'é', 'ú', 'ó', 'Á', 'É', 'Í', 'Ó', 'Ú'), '', $data);
        // return str_replace(' ', '-', $imagen_validad);
        return $imagen_validad;
    }

    public function updatePostVisitas()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body = json_decode(file_get_contents("php://input"), true);
            // $columnas = array(
            //     'idpost'  => $_POST['idpost'],
            //     'visitas' => 'visitas + 1',
            // );
            $filas = $this->ApiServiceModel->updatePostVisita($_POST['idpost']);
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }
    //HOROSCOPO
    public function getHoroscopo()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body  = json_decode(file_get_contents("php://input"), true);
            $filas = $this->ApiServiceModel->getHoroscopo();
            if (isset($filas)) {
                $this->metodos->_resultSql_2($filas);
            }
        }
    }

    // INSERTA EN BASE DE DATOS/
    public function setLoginSuscriptor()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $body   = json_decode(file_get_contents("php://input"), true);
            $sesion = $body['sesion'];
            switch ($sesion) {
                case 'GOOGLE':
                    $email       = $body['correo'];
                    $filaUsuario = $this->ApiServiceModel->getUsuarioRepetido($email);
                    if ($filaUsuario['idsuscriptor'] == null) {
                        $columnas = array(
                            'idsuscriptor' => 'cl_' . date('Ymd_His') . '_' . $this->generarCodigo(5),
                            'nick'         => $body['nick'],
                            'correo'       => $email,
                            'password'     => 'GOOGLE',
                            'top1'         => 0,
                            'top2'         => 0,
                            'logros'       => 'Sabiondo supremo-0_Arriesgado-0_Estudiante-0_Maestro-0_Corcho-0_Desafiante-0_Invicto-0_Glorioso-0_Combatiente de Ciencia-0_Demoledor Historico-0_Sabio del Entretenimiento-0_Erudito del Arte-0_Maestro Geográfico-0',
                            'monedas'      => 40,
                            'f_nacimiento' => '2019-01-01',
                            'f_alta'       => date('Y-m-d'),
                            'f_baja'       => date('Y-m-d'),
                            'estado'       => 'A',
                            'tipo'         => 'Suscriptor',
                            'foto'         => $body['foto'],
                            'premio'       => '0',
                        );
                        $fila = $this->ApiServiceModel->setUsuario($columnas);
                        if (isset($fila)) {
                            $this->metodos->_resultSql_2($columnas);
                        } else {
                            echo json_encode([
                                "Message" => "error",
                                "Status"  => "bad",
                            ]);
                        }
                    } else {
                        $this->metodos->_resultSql_2($filaUsuario);
                    }
                    break;

                case 'SABIONDO':
                    $email = $body['correo'];
                    $fila  = $this->ApiServiceModel->getUsuarioRepetido($email);
                    $this->metodos->_resultSql_2($fila);

                    break;
            }
        }
    }

    public function setSuscriptor()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // $body     = json_decode(file_get_contents("php://input"), true);
            $columnas = array(
                'idsuscriptor' => 'cl_' . date('Ymd_His') . '_' . $this->generarCodigo(5),
                'nick'         => $_POST['nick'],
                'correo'       => $_POST['correo'],
                'password'     => $_POST['password'],
                'top1'         => 0,
                'top2'         => 0,
                'logros'       => 'Sabiondo supremo-0_Arriesgado-0_Estudiante-0_Maestro-0_Corcho-0_Desafiante-0_Invicto-0_Glorioso-0_Combatiente de Ciencia-0_Demoledor Historico-0_Sabio del Entretenimiento-0_Erudito del Arte-0_Maestro Geográfico-0',
                'monedas'      => 40,
                'f_nacimiento' => '1992-01-01',
                'f_alta'       => date('Y-m-d'),
                'f_baja'       => date('Y-m-d'),
                'estado'       => 'A',
                'tipo'         => 'Suscriptor',
                'foto'         => 'IMG_' . date('Ymd_His') . '_' . $this->generarCodigo(5) . ".jpeg",
                'premio'       => '0',
            );

            $fila = $this->ApiServiceModel->setUsuario($columnas);
            if (isset($fila)) {
                echo json_encode([
                    "Message" => "OK",
                    "Status"  => "good",
                ]);
                $image       = $_POST["foto"];
                $upload_path = "public/folder/img_cliente/" . $columnas['foto'];
                file_put_contents($upload_path, base64_decode($image));

                $image       = $_POST["foto_lite"];
                $upload_path = "public/folder/img_cliente/lite/" . $columnas['foto'];
                file_put_contents($upload_path, base64_decode($image));
            } else {
                echo json_encode([
                    "Message" => "error",
                    "Status"  => "bad",
                ]);
            }
        }
    }
}
