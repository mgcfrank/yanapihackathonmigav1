<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('America/La_Paz'); #Línea agregada
class ApiEmpresa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('ApiServiceModel');
        $this->load->library('Metodos');
    }

    public function index()
    {
        //SERVICIO PARA DETECTAR ACTUALIZACIONES O MIGRACIONES, DONDE LA API SOLO DEBE PODER CONSULTAR Y NO AGREGAR, NI EDITAR, NI ELIMINAR
        //servicio para dectectar dispostitivos que utilizan una dicha version o mandar actuaizacion forzoza desde la app
    }

    //USUSARIO Y LOGUEO
    public function getLoginEmpresa()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $filas = $this->ApiServiceModel->getLoginEmpresa($_POST['numero']);
            if (isset($filas)) {
                if ($filas['estado'] != 'Restringido') {
                    $this->metodos->_resultClient(true, "Usuario activo", $filas);
                } else {
                    $this->metodos->_resultClient(false, "Usuario restrinjido, contactese con el administrador", array('estado' => 'Restringido'));
                }
            } else {
                $this->metodos->_resultClient(false, "El usuario no esta registrado", array('estado' => 'Nuevo'));
            }
        }
    }

    public function setEmpresa()
    {
        //oculta las advertencias de codeingiter pra temp
        error_reporting(0);
        ini_set('display_errors', 0);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($this->ApiServiceModel->getExistEmpresaCelular($_POST['numero'])) {
                $this->metodos->_resultClient(false, "El numero ya esta registrado", []);
                return;
            }
            $columnas = array(
                'idEmpresa'          => 'u' . date('Ymd-His', time()) . '-' . $this->metodos->generarCodigo(10),
                'nombre'             => $_POST['nombre'],
                'categorias'         => $_POST['categorias'],
                'descripcion'        => $_POST['nombre'],
                'encargado'          => '',
                'numero'             => $_POST['numero'],
                'whatsapp'           => $_POST['numero'],
                'messenger'          => 'No',
                'foto'               => 'IMG-' . date('Ymd-His', time()) . '-' . $this->metodos->generarCodigo(10) . '.jpg',
                'precio_envio_metro' => -1,
                'delivery'           => 'Si',
                'estado'             => 'Activo',
                'mayorista'          => $_POST['mayorista'],
                'minorista'          => $_POST['minorista'],
                'f_alta'             => date('Y-m-d H:i:s', time()),
                'f_modificacion'     => date('Y-m-d H:i:s', time()),
            );
            //COPIAMOS LA FOTOGRAFIA DEL CLIENTE
            $file2             = $_FILES["foto"];
            $origen2           = $file2["tmp_name"];
            $destino2          = "public/folder/img_empresa_lite/" . $columnas['foto'];
            $destino_temporal2 = tempnam("tmp/", "tmp");

            if ($this->metodos->redimensionarImagen($origen2, $destino_temporal2, 62, 62, 100)) {
                $fp = fopen($destino2, "w");
                fputs($fp, fread(fopen($destino_temporal2, "r"), filesize($destino_temporal2)));
                fclose($fp);
                //copea la imagen original
                $file2             = $_FILES["foto"];
                $origen2           = $file2["tmp_name"];
                $destino_temporal2 = tempnam("tmp/", "tmp");
                $destino2          = "public/folder/img_empresa/" . $columnas['foto'];
                if ($this->metodos->redimensionarImagen($origen2, $destino_temporal2, 640, 640, 60)) {
                    $fp = fopen($destino2, "w");
                    fputs($fp, fread(fopen($destino_temporal2, "r"), filesize($destino_temporal2)));
                    fclose($fp);
                } else {
                    //eliminamos las fotografias para no cargar al servidor
                    $this->metodos->deleteFile("public/folder/img_empresa_lite/" . $columnas['foto']);
                    $this->metodos->deleteFile("public/folder/img_empresa/" . $columnas['foto']);
                    $this->metodos->_resultClient(false, "Red inestable no se pudo guardar su fotografia, intentelo de nuevo.", []);
                    return;
                }
            } else {
                //eliminamos las fotografias para no cargar al servidor
                $this->metodos->deleteFile("public/folder/img_empresa_lite/" . $columnas['foto']);
                $this->metodos->deleteFile("public/folder/img_empresa/" . $columnas['foto']);
                $this->metodos->_resultClient(false, "Red inestable no se pudo guardar su fotografia, intentelo de nuevo.", []);
                return;
            }

            $filas = $this->ApiServiceModel->setEmpresa($columnas);
            if (isset($filas)) {
                $columnasSucrsal = array(
                    'idSucursal ' => 'sg' . date('Ymd-His', time()) . '-' . $this->metodos->generarCodigo(10),
                    'idEmpresa'   => $columnas['idEmpresa'],
                    'direccion'   => '',
                    'latitud'     => $_POST['latitud'],
                    'longitud'    => $_POST['longitud'],
                );
                $filas = $this->ApiServiceModel->setSucursalGps($columnasSucrsal);
                $this->metodos->_resultClient(true, "Cuenta creada exitosamente", $columnas);
            } else {
                $this->metodos->_resultClient(false, "Red inestable, no se regsitro su empresa vuelva a intentarlo mas tarde.", $filas);
                //eliminamos las fotografias para no cargar al servidor
                $this->metodos->deleteFile("public/folder/img_empresa_lite/" . $columnas['foto']);
                $this->metodos->deleteFile("public/folder/img_empresa/" . $columnas['foto']);
                $this->metodos->_resultClient(false, "Red inestable no se pudo guardar su fotografia, intentelo de nuevo.", []);
                return;
            }
        }
    }

    public function updateMayorista()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $columnas = array(
                'idEmpresa' => $_POST['idEmpresa'],
                'mayorista' => $_POST['mayorista'],
            );
            $filas = $this->ApiServiceModel->updateEmpresa($columnas);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Estado mayorista actualizado", $columnas);
            } else {
                $this->metodos->_resultClient(true, "Red inestable, estado mayorista no actualizado", $columnas);
            }
        }
    }

    public function updateMinorista()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $columnas = array(
                'idEmpresa' => $_POST['idEmpresa'],
                'minorista' => $_POST['mayorista'],
            );
            $filas = $this->ApiServiceModel->updateEmpresa($columnas);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Estado minorista actualizado", $columnas);
            } else {
                $this->metodos->_resultClient(true, "Red inestable, estado minorista no actualizado", $columnas);
            }
        }
    }

    public function updateNumero()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($this->ApiServiceModel->getExistEmpresaCelular($_POST['numero'])) {
                $this->metodos->_resultClient(false, "El numero ya esta registrado", []);
                return;
            }
            $columnas = array(
                'idEmpresa' => $_POST['idEmpresa'],
                'numero'    => $_POST['numero'],
            );
            $filas = $this->ApiServiceModel->updateEmpresa($columnas);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Numero actualizado", $columnas);
            } else {
                $this->metodos->_resultClient(true, "Red inestable, numero no actualizado", $columnas);
            }
        }
    }

    public function updateWhatsapp()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $columnas = array(
                'idEmpresa' => $_POST['idEmpresa'],
                'whatsapp'  => $_POST['whatsapp'],
            );
            $filas = $this->ApiServiceModel->updateEmpresa($columnas);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Whatsapp actualizado", $columnas);
            } else {
                $this->metodos->_resultClient(true, "Red inestable, whatsapp no actualizado", $columnas);
            }
        }
    }

    public function updateMessenger()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $columnas = array(
                'idEmpresa' => $_POST['idEmpresa'],
                'messenger' => $_POST['messenger'],
            );
            $filas = $this->ApiServiceModel->updateEmpresa($columnas);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Messenger actualizado", $columnas);
            } else {
                $this->metodos->_resultClient(true, "Red inestable, messenger no actualizado", $columnas);
            }
        }
    }

    public function updateDelivery()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $columnas = array(
                'idEmpresa' => $_POST['idEmpresa'],
                'delivery'  => $_POST['delivery'],
            );
            $filas = $this->ApiServiceModel->updateEmpresa($columnas);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Delivery actualizado", $columnas);
            } else {
                $this->metodos->_resultClient(true, "Red inestable, delivery no actualizado", $columnas);
            }
        }
    }

    public function updateCostoEnvioDelivery()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $columnas = array(
                'idEmpresa'          => $_POST['idEmpresa'],
                'precio_envio_metro' => $_POST['precio_envio_metro'],
            );
            $filas = $this->ApiServiceModel->updateEmpresa($columnas);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Costo delivery actualizado", $columnas);
            } else {
                $this->metodos->_resultClient(true, "Red inestable, costo delivery no actualizado", $columnas);
            }
        }
    }

//:::::::::::::::::::::::::::::::::::::::::::::
    //::::::::      SUCURSALES
    //::::::::::::::::::::::::::::::::::::::::::::
    public function updateSucursalEmpresa()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $columnas = array(
                'idSucursal'       => $_POST['idSucursal'],
                'direccion'        => $_POST['direccion'],
                'latitud'          => $_POST['latitud'],
                'longitud'         => $_POST['longitud'],
                'horario_atencion' => $_POST['horario_atencion'],
                'horario_pedidos'  => $_POST['horario_pedidos'],
            );
            $filas = $this->ApiServiceModel->updateSucursalEmpresa($columnas);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Sucursal actualizado actualizado", $columnas);
            } else {
                $this->metodos->_resultClient(true, "Red inestable, sucursal no no actualizado", $columnas);
            }
        }
    }

    public function getSucursalesEmpresa()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $filas = $this->ApiServiceModel->getSucursalesEmpresa($_POST['idEmpresa']);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Sucursales de empresa: ", $filas);
            } else {
                $this->metodos->_resultClient(false, "No hay sucursales: ", $filas);
            }
        }
    }

    public function getEmpresaGpsCliente()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $lat      = $_POST['latitud'];
            $lng      = $_POST['longitud'];
            $distance = 1; // Sitios que se encuentren en un radio de 1KM
            $box      = $this->metodos->getBoundaries($lat, $lng, $distance);
            $filas    = $this->ApiServiceModel->getEmpresaGpsCliente($_POST['filtro'], $lat, $lng, $box, $distance);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Empresa dentro de mi radio 1km", $filas);
            } else {
                $this->metodos->_resultClient(false, "No hay empresa: ", $filas);
            }
        }
    }

    public function getVistaEmpresaCliente()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $filas = $this->ApiServiceModel->getVistaEmpresaCliente($_POST['idEmpresa'], $_POST['idSucursal']);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Sucursales de empresa: ", $filas);
            } else {
                $this->metodos->_resultClient(false, "No hay sucursales: ", $filas);
            }
        }
    }

    public function getBusquedaEmpresa()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $lat   = $_POST['latitud'];
            $lng   = $_POST['longitud'];
            $filas = $this->ApiServiceModel->getBusquedaEmpresa($_POST['texto'], $lat, $lng);
            if (isset($filas)) {
                $this->metodos->_resultClient(true, "Resultado de la busqueda de: " . $_POST['texto'], $filas);
            } else {
                $this->metodos->_resultClient(false, "No hay resultado de: " . $_POST['texto'], $filas);
            }
        }
    }
}
