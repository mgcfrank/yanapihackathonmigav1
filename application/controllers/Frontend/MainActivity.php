<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('America/La_Paz'); #Línea agregada
class MainActivity extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = array(
            'titulo'      => 'Pagina de inicio',
            'descripcion' => 'Servicios, Productos, Lugares entretenimiento, Negocios, Empresas, La Paz, Cochabamba, Santa Cruz',
            'foto'        => '');
        $this->load->view('component/starPage', $data);
        $this->load->view('cliente/InicioActivity');
    }

    public function PerfilUsuario()
    {
        $data = array(
            'titulo'      => 'Pagina de perfil',
            'descripcion' => 'Servicios, Productos, Lugares entretenimiento, Negocios, Empresas, La Paz, Cochabamba, Santa Cruz',
            'foto'        => '');
        $this->load->view('component/starPage', $data);
        $this->load->view('cliente/PerfilActivity');
    }
}
