<style type="text/css">
    #header{
      background-image: url("<?php echo base_url() ?>public/images/background-quibo-developer.jpg");
      background-position: center center;
    }
    #header .menu-item{
      color: #fff;
      font-size: 17px;
      text-align: center;
      padding-top: 15px;
      padding-top: 10px;
      padding-bottom: 10px;
    }
    #header .menu-item:hover{
      background-color: #fff2;
      border-radius: 12px 12px 0 0;
    }
    #header .active{
      background: #fff4;
      border-radius: 12px 12px 0 0;
    }
    #header .no-active{
      background: #fff0;
    }

    #header .input-style{
      background: #fff;
      width: 100%;
      border-radius: 20px;
      border: none;
      background-color: #ffffffba;
      outline: none !important;
      padding: 5px 10px;
    }
    /*//mapa*/
    #container_maps{
       z-index: 1;
      /*background: red;*/
      /*height: 100vh*/
    }

    #item_filtro{
      background: rgb(254, 211, 23);
      border-radius: 20px;
      color: #10152f;
      font-weight: bold;
      line-height: 13px;
      padding: 5px;
      text-align: center;
      justify-content: center;
      align-items: center;
    }

    #item_filtro img{
      /*width: 88px;*/
      height: 80px;
      max-width: 120px;
    }

    /*//vista empresa*/
    #modalVistaEmpresa .foto{
      height: 60px;
      width: 60px;
      border-radius: 50%;
    }

    #modalVistaEmpresa .titulo{
      line-height: 0.9;
    }

    #modalVistaEmpresa .horario{
      font-size: 13px;
      margin-top: -10px;
      margin-bottom: 4px;
    }
    #modalVistaEmpresa .mayorista{
      background: #438ec3;
      padding: 4px;
      border-radius: 15px;
      color: #fff;
    }
     #modalVistaEmpresa .estado{
      width: 100%;
     }
    #modalVistaEmpresa .descripcion{
     font-size: 14px;
     padding: 4px;
    }
    #modalVistaEmpresa .buttonModal{
      bottom: 15px;
      right: 15px;
      padding: 10px;
      border-radius: 20px;
      background: rgb(242 170 10);
      color: #fff
    }

    #modalVistaEmpresa .fotoProducto{
      height: 45px;
      width: 45px;
      border-radius: 20%;
    }

    #modalVistaEmpresa .tituloProducto{
      line-height: 0.9;
      color: #8a8a8a;
      font-size: 16px;
    }

    #modalVistaEmpresa .precioProducto{
      line-height: 0.9;
      font-size: 22px;
      color: #5d5a5a;
    }
    /*//catalogo de empresa*/
    #modalCatalogoEmpresa .foto{
      height: 60px;
      width: 60px;
      border-radius: 50%;
    }

    #modalCatalogoEmpresa .titulo{
      line-height: 0.9;
    }

    #modalCatalogoEmpresa .horario{
      font-size: 13px;
      margin-top: -10px;
      margin-bottom: 4px;
    }
    #modalCatalogoEmpresa .mayorista{
      background: #438ec3;
      padding: 4px;
      border-radius: 15px;
      color: #fff;
    }

    #modalCatalogoEmpresa .fotoProducto{
      height: 60px;
      width: 60px;
      border-radius: 20%;
    }

    #modalCatalogoEmpresa .tituloProducto{
      line-height: 0.9;
      color: #8a8a8a;
      font-size: 16px;
    }

    #modalCatalogoEmpresa .precioProducto{
      line-height: 0.9;
      font-size: 15px;
      font-weight: bold;
      color: #5d5a5a;
    }

    #modalCatalogoEmpresa .buttonModal{
      bottom: 15px;
      right: 15px;
      padding: 10px;
      border-radius: 20px;
      background: rgb(242 170 10);
      color: #fff
    }

     #modalCatalogoEmpresa .buttonModalInv{
      bottom: 15px;
      right: 15px;
      padding: 10px;
      border-radius: 20px;
      font-weight: bold;
      color: rgb(242 170 10);
      border-width: 1px;
      border-style: solid;
      border-color: rgb(242 170 10);
    }

    /*//items de busqueda*/
    #itemBusqueda .titulo{
      line-height: 0.9;
    }

    #itemBusqueda .ditancia{
      font-size: 0.75em;
      margin-bottom: 4px;
      color: #28a745;
    }
    #itemBusqueda .mayorista{
      background: #438ec3;
      padding: 4px;
      border-radius: 15px;
      color: #fff;
      font-size: 0.7em;
    }

    #itemBusqueda .minorista{
      background: #deb934;
      padding: 4px;
      border-radius: 15px;
      color: #fff;
      font-size: 0.6em;
    }

    #itemBusqueda .direccion{
      font-size: 0.8em;
      color: #868686;
    }

    .itemTemporada{
      bottom: 15px;
      right: 15px;
      padding: 10px;
      border-radius: 10px;
      font-weight: bold;
      color: #4c3939;
      border-width: 1px;
      border-style: solid;
      background: rgb(255 244 221 / 77%);
    }

  </style>

</head>
<body>
  <section  id="app">
    <!-- //menu -->
    <div class="container-fluid" id="header">
      <div class="row">
        <div class="col-2 col-sm-2 col-md-1 col-lg-1 col-xl-1" style="padding-top: 5px; text-align: right;">
          <img src="<?php echo base_url() ?>public/images/icono-quibo-logo-bolivia.png" width="50">
        </div>
        <div class="col-10 col-sm-10 col-md-3 col-lg-3 col-xl-3"  style="margin-top: 15px;">
          <input class="input-style" type="text" v-model="textBuscar" v-on:keyup.enter="buscarTexto()" placeholder="¿Que estas buscando?">
        </div>
        <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
          <!-- vacio -->
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" style="padding: 0px;">
          <div class="container-fluid">
            <div class="row" style="margin-top: 15px;">
              <!-- //mostrar solo en dispositivo movil: d-block d-sm-block d-md-none-->
             <!--  <div class="col-6 menu-item d-block d-sm-block d-md-none">
                Cate
                <div class="no-active"></div>
              </div> -->
              <a class="col-6 menu-item active" href="<?php echo base_url() ?>Inicio">
                <i class="fas fa-house-damage"></i>Tiendas cercanas
              </a>
              <a class="col-6 menu-item no-active" href="<?php echo base_url() ?>Perfil">
                <i class="fas fa-users-cog"></i> Mi perfil
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- //final de menu -->
    <div class="container-fluid container_maps" id="container_maps">
      <!-- Button trigger modal -->
      <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
        Launch demo modal
      </button> -->
<!--       <google-map style="width: 100%; height: 100%; position: absolute; left:0; top:0"
          :center="{lat: 1.38, lng: 103.8}"
          :zoom="12"
      ></google-map> -->
    </div>

    <!-- Modal categoria-->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" style="background-color: #fff0; border: 1px solid rgb(0 0 0 / 0%);">
          <div class="container">
            <div class="row">
              <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="padding: 10px" v-on:click="iniciaMapa('Restaurante y cafeterias')">
                <div id="item_filtro">
                  <img src="<?php echo base_url() ?>public/folder/img_categoria/yanapi-restaurantes.png">
                  <p>Restaurante y cafeterias</p>
                </div>
              </div>
              <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="padding: 10px" v-on:click="iniciaMapa('Tiendas y abarrotes')">
                <div id="item_filtro">
                  <img src="<?php echo base_url() ?>public/folder/img_categoria/yanapi-tiendas.png">
                  <p>Tiendas  y abarrotes</p>
                </div>
              </div>
              <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="padding: 10px" v-on:click="iniciaMapa('Panaderias y Pastelerias')">
                <div id="item_filtro">
                  <img src="<?php echo base_url() ?>public/folder/img_categoria/yanapi-panaderias.png">
                  <p>Panaderias y Pastelerias</p>
                </div>
              </div>
              <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="padding: 10px" v-on:click="iniciaMapa('Frutas y verduras')">
                <div id="item_filtro">
                  <img src="<?php echo base_url() ?>public/folder/img_categoria/yanapi-zanahoria.png">
                  <p>Frutas y  verduras</p>
                </div>
              </div>
              <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="padding: 10px" v-on:click="iniciaMapa('Todos')">
                <div id="item_filtro" style="background: #fff">
                  <img src="<?php echo base_url() ?>public/folder/img_categoria/yanapi-lo-que-sea.png">
                  <p>Lo que sea</p>
                </div>
              </div>
              <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="padding: 10px" v-on:click="iniciaMapa('Naturistas  y chiflerias')">
                <div id="item_filtro">
                  <img src="<?php echo base_url() ?>public/folder/img_categoria/yanapi-naturistas.png">
                  <p>Naturistas y chiflerias</p>
                </div>
              </div>
              <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="padding: 10px" v-on:click="iniciaMapa('Carnicerias')">
                <div id="item_filtro">
                  <img src="<?php echo base_url() ?>public/folder/img_categoria/yanapi-carniceria.png">
                  <p>Carnicerias</p>
                </div>
              </div>
              <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="padding: 10px" v-on:click="iniciaMapa('Farmacias')">
                <div id="item_filtro">
                  <img src="<?php echo base_url() ?>public/folder/img_categoria/yanapi-farmacias.png">
                  <p>Farmacias</p>
                </div>
              </div>
              <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="padding: 10px" v-on:click="iniciaMapa('Ferreterias')">
                <div id="item_filtro">
                  <img src="<?php echo base_url() ?>public/folder/img_categoria/yanapi-ferreterias.png">
                  <p>Ferreterias</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal categoria-->
    <div class="modal fade" id="modalTemporada" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" style="background-color: #fff0; border: 1px solid rgb(0 0 0 / 0%);">
          <div class="container">
            <div class="modal-header" style="color: #fff;">
              <h5 class="modal-title" id="exampleModalLongTitle" style="font-weight: normal"><i class="fas fa-calendar"></i> Selecciona una temporadao</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="row" style="margin-top: 5px" v-for="item in arrayTemporada">
              <div style="display: flex; width: 100%" class="itemTemporada">
                <span class="badge badge-dark">{{(item.fecha).split("-")[1]}}</span>
                <span class="badge badge-success" v-if="item.estado=='Abundante'">{{item.estado}}</span>
                <span class="badge badge-warning" v-if="item.estado=='Normal'">{{item.estado}}</span>
                <span class="badge badge-danger" v-if="item.estado=='Escaso'">{{item.estado}}</span>
                <div style="padding-left: 10px;">{{item.titlo}}</div>
              </div>
            </div>

        <!--     <div class="row" style="margin-top: 5px">
              <div style="display: flex; width: 100%" class="itemTemporada">
                <i class="far fa-calendar" style="font-size: 1.5rem;"></i>
                <div style="padding-left: 10px;">Mis perfil</div>
              </div>
            </div> -->

          </div>
        </div>
      </div>
    </div>

    <!-- MODAL VISTA EMPRESA -->
    <div class="modal fade" id="modalVistaEmpresa" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <div class="row">
              <div class="col-3 col-sm-3 col-md-3 col-lg-2 col-xl-2" style="text-align: right;">
                <img class="foto" v-bind:src="BASE_URL+'public/folder/img_empresa_lite/'+arrayVistaEmpresa.foto">
              </div>
              <div class="col-7 col-sm-7 col-md-7 col-lg-8 col-xl-8">
                <h5 class="titulo">{{arrayVistaEmpresa.nombre}}</h5>
                <p class="horario">Atencion de 9 a 12</p>
                <span class="mayorista" v-if="arrayVistaEmpresa.mayorista=='Si'">Mayorista</span>
                <span class="mayorista" v-if="arrayVistaEmpresa.minorista=='Si'">Minorista</span>
              </div>
              <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2" style="padding: 0px">
                <img class="estado" src="<?php echo base_url() ?>public/images/img_empresa_closed.png">
              </div>
            </div>
            <div class="row descripcion">
              {{arrayVistaEmpresa.descripcion}}
            </div>
            <div class="row">
              <a class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2" v-bind:href="'tel:'+arrayVistaEmpresa.numero" style="text-align: center; padding: 2px" target="_blank">
                <div class="buttonModal"><i class="fas fa-phone" style="font-size: 25px;"></i></div>
              </a>
              <a class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2" v-bind:href="'https://api.whatsapp.com/send?phone=591'+arrayVistaEmpresa.whatsapp" target="_blank" style="text-align: center; padding: 2px">
                <div class="buttonModal"><i class="fab fa-whatsapp" style="font-size: 25px;"></i></div>
              </a>
              <a class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2" v-bind:href="'https://m.me/'+arrayVistaEmpresa.messenger" target="_blank" style="text-align: center; padding: 2px" v-if="arrayVistaEmpresa.messenger!='No'">
                <div class="buttonModal"><i class="fab fa-facebook-messenger" style="font-size: 25px;"></i></div>
              </a>
              <div class="col-6 col-sm-6 col-md-2 col-lg-2 col-xl-6" style="text-align: center; padding: 2px;font-size: 14px;line-height: 1;" v-on:click="cargaCatalogoEmpresa(arrayVistaEmpresa.idEmpresa)">
                <div class="buttonModal">¿Ver tienda y <br> realizar pedido?</div>
              </div>
            </div>
            <h6 style="margin-top: 10px;margin-bottom: 10px;">Alguno de los productos que te ofrezco</h6>
            <div class="row" style="margin-top: 12px;" v-for="item in arrayVistaEmpresaProducto">
              <div class="col-2 col-sm-2 col-md-3 col-lg-2 col-xl-2" style="text-align: right; padding: 2px;">
                <img class="fotoProducto" v-bind:src="BASE_URL+'public/folder/img_producto/'+item.foto">
              </div>
              <div class="col-7 col-sm-7 col-md-7 col-lg-7 col-xl-7">
                <div class="tituloProducto">{{item.titulo}}</div>
              </div>
              <div class="col-3 col-sm-3 col-md-2 col-lg-3 col-xl-3" style="padding: 0px">
                <h6 class="precioProducto">{{item.precio}} Bs.</h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

     <!-- MODAL VISTA PRODUCTO EMPRESA -->
    <div class="modal fade" id="modalCatalogoEmpresa" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
        <div class="modal-content" style="background: #ececec">
          <div class="modal-body">
            <div class="row">
              <div class="col-3 col-sm-3 col-md-3 col-lg-2 col-xl-2" style="text-align: right;">
                <img class="foto" v-bind:src="BASE_URL+'public/folder/img_empresa_lite/'+arrayVistaEmpresa.foto">
              </div>
              <div class="col-9 col-sm-9 col-md-9 col-lg-10 col-xl-10">
                <h5 class="titulo">{{arrayVistaEmpresa.nombre}}</h5>
                <p class="horario">Atencion de 9 a 12</p>
                <span class="mayorista" v-if="arrayVistaEmpresa.mayorista=='Si'">Mayorista</span>
                <span class="mayorista" v-if="arrayVistaEmpresa.minorista=='Si'">Minorista</span>
              </div>
            </div>
            <h6 style="margin-top: 20px;margin-bottom: 20px;">Catalogo de productos</h6>
            <div class="container">
              <div class="row">
                <div class="col-4 col-sm-4 col-md-3 col-lg-3 col-xl-3" v-for="(item, index) in arrayCatalogoEmpresa" style="text-align: center;padding: 5px;">
                  <img class="fotoProducto" v-bind:src="BASE_URL+'public/folder/img_producto/'+item.foto">
                  <div class="tituloProducto"><strong>Cant. {{item.stock}}</strong><br> {{item.titulo}}</div>
                  <h6 class="precioProducto">{{item.precio}} Bs.</h6>
                  <div style="display: flex;">
                    <button style="width: 27%;" v-on:click="quitaProducto(index)">-</button>
                    <input type="text" v-model="item.carro" style="width: 46%; text-align: center;">
                    <button style="width: 27%;" v-on:click="agregaProducto(index)">+</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="text-align: center; padding: 2px;font-size: 14px;line-height: 1;">
                <div class="buttonModalInv"><i class="fas fa-shopping-cart"></i> Cant:{{cantidadCarrito}} - Precio: {{totalCarrito}}bs</div>
              </div>
              <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="text-align: center; padding: 2px;font-size: 14px;line-height: 1;"  v-on:click="enviarPedidoWhatsapp()">
                <div class="buttonModal">Pedido por whatsapp</div>
              </div>
              <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="text-align: center; padding: 2px;font-size: 14px;line-height: 1;" v-on:click="enviarPedidoYanapi()">
                <div class="buttonModal">Pedido por yanapi</div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <!-- MODAL VISTA PRODUCTO EMPRESA -->
    <div class="modal fade" id="modalResultadoBusqueda" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
        <div class="modal-content" style="background: #ececec">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle" style="font-weight: normal"> <i class="fas fa-search"></i> Resultado de la busqueda de: <strong>{{textBuscar}}</strong></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row" id="itemBusqueda" style="margin-top: 3px;background: #ffffff40;" v-for="item in arrayBusqueda" v-on:click="agregaEmpresa(item)">
              <div class="col-2 col-sm-2 col-md-3 col-lg-2 col-xl-2" style="text-align: right; padding: 2px;">
                <img class="fotoProducto" v-bind:src="BASE_URL+'public/folder/img_empresa_lite/'+item.foto">
              </div>
              <div class="col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10">
                <div class="titulo">{{item.nombre}}</div>
                <div class="ditancia"><i class="fas fa-map-marker-alt"></i> Esta a <STRONG>{{parseFloat(item.distance).toFixed(3)}}</STRONG> km.</div>
                <div>
                  <span class="mayorista" v-if="item.mayorista=='Si'"><i class="fas fa-cubes"></i> Mayorista</span>
                  <span class="minorista" v-if="item.minorista=='Si'"><i class="fas fa-cube"></i> Minorista</span>
                </div>
                <div class="direccion">{{item.direccion}}</div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <!-- MODAL DE FINALIZACION DE PEDIDO -->
    <div class="modal fade" id="modalFinalizaPedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="background: #dadada">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Detalle del pedido</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="input-group input-group-sm mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm">Detalle</span>
              </div>
              <input type="text" class="form-control" v-model="carritoDetalle" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
            </div>

            <div class="input-group input-group-sm mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm">Direccion</span>
              </div>
              <input type="text" class="form-control" v-model="carritoDireccion" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
            </div>

            <div class="input-group input-group-sm mb-3">
              <input type="text" class="form-control" v-model="carritoGps" v-on:keyup="cargaGps()" placeholder="Recipient's username" aria-label="Ej: -8.13123, -60.12312" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="button" v-on:click="modalUbicacionPedido()">Mapa</button>
              </div>
            </div>

            <div style="display: flex; font-weight: bold;" >
              <div style="width: 27%;">Cant.</div>
              <div style="width: 46%;">Producto</div>
              <div style="width: 27%;">Precio</div>
            </div>
            <div style="display: flex;" v-for="(item, index) in arrayCarritoCompra">
              <div style="width: 27%;">{{item.catidad}}</div>
              <div style="width: 46%;">{{item.titulo_producto}}</div>
              <div style="width: 27%;">{{item.precio_actual*item.catidad}} bs.</div>
            </div>
            <div style="display: flex;">
              <div style="width: 73%; text-align: right; font-weight: bold;">Precio total de productos: </div>
              <div style="width: 27%;">{{totalCarrito}} Bs.</div>
            </div>
            <div style="display: flex;">
              <div style="width: 73%; text-align: right; font-weight: bold;">Precio total del envio: </div>
              <div style="width: 27%;">{{totalDelivery}} Bs.</div>
            </div>
            <div style="display: flex; font-weight: bold;">
              <div style="width: 73%; text-align: right;">TOTAL: </div>
              <div style="width: 27%;">{{parseFloat(totalCarrito)+parseFloat(totalDelivery)}} Bs.</div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary" v-on:click="registrarPedido()">Enviar pedido</button>
          </div>
        </div>
      </div>
    </div>

    <!-- MODAL VISTA PRODUCTO EMPRESA -->
    <div class="modal fade" id="modalUbicacionPedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
        <div class="modal-content" style="background: #ececec">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle" style="font-weight: normal"><i class="fas fa-map-marker-alt"></i> Selecciona la ubicacion para tu pedido</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" style="height: 300px;" id="mapaSeleccion">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Seleccionar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- MODAL DE ALERTA DE USUSARIO NO LOGUEADO -->
    <div class="modal fade" id="modalSinUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="background:#f5da8a">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" style="text-align: center;">
            <h5>Debes iniciar sesion para realizar este pedido</h5>
            <div>
              Para poder realizar un pedido primero debes iniciar session o crear un cuenta  .<br><br>.No te preocupes que es muy facil.<br><br>
            </div>
            <a style="
              bottom: 15px;
              right: 15px;
              padding: 10px;
              border-radius: 20px;
              background: rgb(242 170 10);
              color: #fff;" href="<?php echo base_url() ?>Perfil">
               Iniciar sesion o registrate
            </a>
          </div>
        </div>
      </div>
    </div>


    <!-- MENU FLOTANTE -->
    <div style="
      display: scroll;
      position: fixed;
      z-index: 2;
      bottom: 15px;
      right: 15px;
      padding: 10px;
      border-radius: 20px;
      background: rgb(242 170 10);
      color: #fff;" data-toggle="modal" data-target="#exampleModalCenter">
        <i class="fas fa-search"></i>Que estas buscando
    </div>
    <!-- MENU FLOTANTE -->
    <div style="
      display: scroll;
      position: fixed;
      z-index: 2;
      bottom: 15px;
      left: 15px;
      padding: 10px;
      border-radius: 20px;
      background: rgb(242 170 10);
      color: #fff;" data-toggle="modal" data-target="#modalTemporada">
        <i class="far fa-calendar-alt"></i> Temporadas
    </div>
  </section>
  <script type="text/javascript">
    function initSizeScreen() {
      //redimenciona la ventana de mapa deacuerdo al alto del navegador y del header
      var altoWin=$(window).height();
      var anchoWin=$(window).width();
      var objHeader = document.getElementById('header');
      var objMapa = document.getElementById('container_maps');
      var altoMapa=parseInt(parseInt(altoWin)-objHeader.clientHeight);
      objMapa.style.height = altoMapa+'px';
      // console.log("altoWin: "+altoWin+", altoHeader:"+objHeader.clientHeight+"  altomMapa:"+altoMapa);
    }
    //inicia la primera configuracion de patanlla tanto para el header como para el mapa
    initSizeScreen();
    //se llama a la funcion que detecta el cambio de la pantalla
    $(window).resize(function(){
      // cada vez que se cambie la pantalla se llama a la funcion que redimensiona el mapa y header
      initSizeScreen();
    });


    //iniciarmos el local STORAGE
    if(typeof(Storage)!='undefined'){
      //el navegador es compatible
      console.log("storage init");

    }else{
      //el navegador no es compatible
      console.log("storage fail");
    }

    //iniciarmos nuestro aplicacion con VUE-JS
    const app = new Vue({
      el: '#app',
      data: {
        textBuscar:'',
        arrayBusqueda:[],
        itemEmpresaBusqueda:{},
        //recupera mapa y tiendas
        arrayTiendas:[],
        map:null,
        markersLayer: new L.LayerGroup(),
        latitude:0,
        longitude:0,
        //vista empresa
        latitudEmpresa:0,
        longitudEmpresa:0,
        arrayVistaEmpresa:{},
        arrayVistaEmpresaSucursal:[],
        arrayVistaEmpresaProducto:[],
        //catalogo de emepresa
        arrayCatalogoEmpresa:[],
        totalCarrito:0,
        cantidadCarrito:0,
        totalDelivery:0,
        arrayCarritoCompra:[],//this.arrayCarritoCompra.push({"idProducto":"p20200717-085020-kh5bs2ivew","catidad":3,"precio_actual": 4.6, "titulo_producto": "Producto 1"});
        carritoDetalle:'',
        carritoDireccion:'',
        carritoGps:'',
        //mapa gps seleccion
        mapaSeleccion:null,
        markersLayerSeleccion:new L.LayerGroup(),
        markerSeleccion:null,
        //temporada
        arrayTemporada:[//prudcion
          {"titlo": "Acelga", "fecha":'2020-07-15', "estado": "Normal"},
          {"titlo": "Apio", "fecha":'2020-07-15', "estado": "Normal"},
          {"titlo": "Berengena", "fecha":'2020-07-15', "estado": "Abundante"},
          {"titlo": "Calabacin", "fecha":'2020-07-15', "estado": "Abundante"},
          {"titlo": "Zapallo", "fecha":'2020-07-15', "estado": "Normal"},
          {"titlo": "Papa", "fecha":'2020-07-15', "estado": "Normal"},
          {"titlo": "Cebolla", "fecha":'2020-07-15', "estado": "Escaso"},
          {"titlo": "Espinaca", "fecha":'2020-07-15', "estado": "Escaso"},
          {"titlo": "Pepino", "fecha":'2020-07-15', "estado": "Normal"},
          {"titlo": "Pimenton", "fecha":'2020-07-15', "estado": "Normal"},
          {"titlo": "Tomate", "fecha":'2020-07-15', "estado": "Normal"},
          {"titlo": "Remolacha", "fecha":'2020-07-15', "estado": "Escaso"},
          {"titlo": "Zanahoria", "fecha":'2020-07-15', "estado": "Normal"},

          {"titlo": "Acelga", "fecha":'2020-08-01', "estado": "Normal"},
          {"titlo": "Apio", "fecha":'2020-08-01', "estado": "Normal"},
          {"titlo": "Berengena", "fecha":'2020-08-01', "estado": "Abundante"},
          {"titlo": "Calabacin", "fecha":'2020-08-01', "estado": "Abundante"},
          {"titlo": "Zapallo", "fecha":'2020-08-01', "estado": "Normal"},
          {"titlo": "Papa", "fecha":'2020-08-01', "estado": "Escaso"},
          {"titlo": "Cebolla", "fecha":'2020-08-01', "estado": "Escaso"},
          {"titlo": "Espinaca", "fecha":'2020-08-01', "estado": "Escaso"},
          {"titlo": "Pepino", "fecha":'2020-08-01', "estado": "Normal"},
          {"titlo": "Pimenton", "fecha":'2020-08-01', "estado": "Normal"},
          {"titlo": "Tomate", "fecha":'2020-08-01', "estado": "Normal"},
          {"titlo": "Remolacha", "fecha":'2020-08-01', "estado": "Escaso"},
          {"titlo": "Zanahoria", "fecha":'2020-08-01', "estado": "Normal"},
        ],
      },
      mounted(){
        this.map = L.map('container_maps');
        this.iniciaMapa("Todos");
      },
      methods: {
        agregaEmpresa(itemEmpresa){
          this.itemEmpresaBusqueda=itemEmpresa;
          console.log("Empresa item: "+itemEmpresa.nombre);
          $('#modalResultadoBusqueda').modal('hide');
          var marker= L.marker([parseFloat(itemEmpresa.latitud), parseFloat(itemEmpresa.longitud)], { icon: this.getIconCategoria(itemEmpresa.categorias)}).bindPopup(itemEmpresa.direccion).openPopup();
          this.markersLayer.addLayer(marker.on('click', function(e) {
                $('#modalVistaEmpresa').modal('show');
                //accedemos por jquery a la funcion creada en vuejs
                $(document).ready(function(){
                    console.log(app.itemEmpresaBusqueda.idSucursal);
                    app.latitudEmpresa=app.itemEmpresaBusqueda.latitud;
                    app.longitudEmpresa=app.itemEmpresaBusqueda.longitud;
                    app.buscarEmpresa(app.itemEmpresaBusqueda.idEmpresa, app.itemEmpresaBusqueda.idSucursal);
                });
              }));
          this.map.flyTo([parseFloat(itemEmpresa.latitud), parseFloat(itemEmpresa.longitud)], 17);
        },
        buscarTexto: function(){
           $('#modalResultadoBusqueda').modal('show');
           var formData = new FormData();
           formData.append("texto", this.textBuscar);
           formData.append("latitud", this.latitude);
           formData.append("longitud", this.longitude);
           this.$http.post(
             BASE_URL+'Backend/ApiEmpresa/getBusquedaEmpresa',
             formData,
             // new FormData(this.$refs.formAddArticulo),
           ).then(function(respuesta) {
             console.log(respuesta.body);
             if (respuesta.body['status']) {
              this.arrayBusqueda=respuesta.body['data'];
              this.vueToast(respuesta.body['message'], 'success');
             }else{
               this.vueToast(respuesta.body['message'], 'error');
             }
           }, function(respuesta) {
             console.log(respuesta.body);
           });
        },
        enviarPedidoWhatsapp(){

        },
        registrarPedido(){
          console.log(JSON.stringify(this.arrayCarritoCompra));
          // return;
          //recupera las tiendas segun la coord y filtro
          var formData = new FormData();
          formData.append("titulo", this.carritoDetalle);
          formData.append("precio_envio", this.totalDelivery);
          formData.append("precio_pedido", this.totalCarrito);
          formData.append("direccion", this.carritoDireccion);
          formData.append("latitud", parseFloat(this.carritoGps.split(",")[0]));
          formData.append("longitud", parseFloat(this.carritoGps.split(",")[1]));
          formData.append("idCliente", localStorage.idCliente);
          formData.append("idEmpresa", this.arrayVistaEmpresa.idEmpresa);
          formData.append("detalle", JSON.stringify(this.arrayCarritoCompra));
          this.$http.post(
            BASE_URL+'Backend/ApiPedido/setPedidoCliente',
            formData,
            // new FormData(this.$refs.formAddArticulo),
          ).then(function(respuesta) {
            $('#modalFinalizaPedido').modal('hide');
            $('#modalCatalogoEmpresa').modal('hide');
            console.log(respuesta.body);
            if (respuesta.body['status']) {
              this.vueToast(respuesta.body['message'], 'success');
            }else{
              this.vueToast(respuesta.body['message'], 'error');
            }
          }, function(respuesta) {
            console.log(respuesta.body);
          });
        },
        modalUbicacionPedido(){
          $('#modalUbicacionPedido').modal('show');
          //pide permiso de ubicacion al navegador
          navigator.geolocation.getCurrentPosition(
            (pos) => {
              const { coords } = pos;
              const { latitude, longitude } = coords;
              //carga el mapa
              this.mapaSeleccion = L.map('mapaSeleccion').setView([latitude, longitude], 17);
              L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              }).addTo(this.mapaSeleccion);
              let iconMarker = L.icon({
                  iconUrl: '<?php echo base_url() ?>public/marker/navigation.png',
                  iconSize: [60, 60],
                  iconAnchor: [30, 60]
              })
              this.mapaSeleccion.on('click', function(e){
                var coord = e.latlng;
                var lat = coord.lat;
                var lng = coord.lng;
                $(document).ready(function(){
                    app.markerSeleccion.setLatLng(e.latlng);
                    app.mapaSeleccion.setView(app.markerSeleccion.getLatLng(),17);
                    app.carritoGps=lat+","+lng;
                    app.cargaGps();
                    console.log("You clicked the map at latitude: " + lat + " and longitude: " + lng);
                });
              });
              //crea un marker con las coordenadas
              this.markerSeleccion=L.marker([latitude, longitude], { icon: iconMarker })
              .addTo(this.mapaSeleccion)
              .openPopup();
            },
            (error) => {
              console.log(error)
            },
            {
              enableHighAccuracy: true,
              timeout: 5000,
              maximumAge: 0
          });
        },
        cargaGps(){
          var cooFinal=this.carritoGps.split(",");
          this.totalDelivery=((this.getCoordenadasMetros(this.latitudEmpresa, this.longitudEmpresa, cooFinal[0], cooFinal[1]))*this.arrayVistaEmpresa.precio_envio_metro).toFixed(2);
        },
        enviarPedidoYanapi(){
          //verifica que exista un ususario
          if(localStorage.idCliente==null){
            $('#modalSinUsuario').modal('show');
            return;
          }else{
            $('#modalFinalizaPedido').modal('show');
            console.log("ususario cargado: "+localStorage.numero);
            //filtra carrito
            this.arrayCarritoCompra=[];
            for (var i = 0; i < this.arrayCatalogoEmpresa.length; i++) {
              if (this.arrayCatalogoEmpresa[i].carro>0) {
                this.arrayCarritoCompra.push({"idProducto":this.arrayCatalogoEmpresa[i].idProducto,"catidad":this.arrayCatalogoEmpresa[i].carro,"precio_actual": parseFloat(this.arrayCatalogoEmpresa[i].precio), "titulo_producto": this.arrayCatalogoEmpresa[i].titulo});
              }
            }
          }
        },
        sumaCarrito(){
          this.totalCarrito=0;
          this.cantidadCarrito=0;
          for (var i = 0; i < this.arrayCatalogoEmpresa.length; i++) {
            if (this.arrayCatalogoEmpresa[i].carro>0) {
              this.totalCarrito=this.totalCarrito+(this.arrayCatalogoEmpresa[i].precio*this.arrayCatalogoEmpresa[i].carro);
              this.cantidadCarrito=this.cantidadCarrito+this.arrayCatalogoEmpresa[i].carro;
            }
          }
        },
        quitaProducto(posicion){
          if (this.arrayCatalogoEmpresa[posicion].carro > 0) {
              this.arrayCatalogoEmpresa[posicion].carro--;
              this.sumaCarrito();
          }else{
            this.vueToast("La cantidad debe ser mayor a 0", 'warnig');
          }
        },
        agregaProducto(posicion){
          if (this.arrayCatalogoEmpresa[posicion].carro < this.arrayCatalogoEmpresa[posicion].stock) {
              this.arrayCatalogoEmpresa[posicion].carro++;
              this.sumaCarrito();
          }else{
            this.vueToast("Stock de producto vacio", 'warnig');
          }
        },
        cargaCatalogoEmpresa: function(idEmpresa){
          console.log(idEmpresa);
          //recupera las tiendas segun la coord y filtro
          var formData = new FormData();
          formData.append("filtro", 'Todos');
          formData.append("idEmpresa", idEmpresa);
          this.$http.post(
            BASE_URL+'Backend/ApiProducto/getProductosEmpresa',
            formData,
            // new FormData(this.$refs.formAddArticulo),
          ).then(function(respuesta) {
            $('#modalCatalogoEmpresa').modal('show');
            //muestra modal de catalogo
            console.log(respuesta.body);
            if (respuesta.body['status']) {
              //recupera las tiendas en un array
              this.arrayCatalogoEmpresa=respuesta.body['data'];
              // for (var i = 0; i < this.arrayTiendas.length; i++) {
              //   this.arrayCarritoCompra.push({"idProducto":this.arrayTiendas[i].idProducto,"catidad":0,"precio_actual": this.arrayTiendas[i].precio, "titulo_producto": this.arrayTiendas[i].titulo});
              // }
              this.vueToast(respuesta.body['message'], 'success');
            }else{
              this.vueToast(respuesta.body['message'], 'error');
            }
          }, function(respuesta) {
            console.log(respuesta.body);
          });
        },
        buscarEmpresa: function(idEmpresa, idSucursal){
          //recupera las tiendas segun la coord y filtro
          var formData = new FormData();
          formData.append("idEmpresa", idEmpresa);
          formData.append("idSucursal", idSucursal);
          this.$http.post(
            BASE_URL+'Backend/ApiEmpresa/getVistaEmpresaCliente',
            formData,
            // new FormData(this.$refs.formAddArticulo),
          ).then(function(respuesta) {
            if (respuesta.body['status']) {
              //recupera las tiendas en un array
              var dataJson=respuesta.body['data'];
              this.arrayVistaEmpresa=dataJson['empresa'];
              this.arrayVistaEmpresaProducto=dataJson['productos'];
              this.arrayVistaEmpresaSucursal=dataJson['sucursal'];
              this.vueToast(respuesta.body['message'], 'success');
            }else{
              this.vueToast(respuesta.body['message'], 'error');
            }
          }, function(respuesta) {
            console.log(respuesta.body);
          });
        },
        cargaTiendas(filtro){
          //oculta modal si esta habierto
          $('#exampleModalCenter').modal('hide');
          //recupera las tiendas segun la coord y filtro
          var formData = new FormData();
          formData.append("latitud", this.latitude);
          formData.append("longitud", this.longitude);
          formData.append("filtro", filtro);
          this.$http.post(
            BASE_URL+'Backend/ApiEmpresa/getEmpresaGpsCliente',
            formData,
            // new FormData(this.$refs.formAddArticulo),
          ).then(function(respuesta) {
            if (respuesta.body['status']) {
              //recupera las tiendas en un array
              this.arrayTiendas=respuesta.body['data'];
              for (var i = 0; i < this.arrayTiendas.length; i++) {
                console.log(this.arrayTiendas[i].latitud);
                // var item=this.arrayTiendas[i];
                var marker= L.marker([parseFloat(this.arrayTiendas[i].latitud), parseFloat(this.arrayTiendas[i].longitud)], { icon: this.getIconCategoria(this.arrayTiendas[i].categorias)}).bindPopup(this.arrayTiendas[i].direccion).openPopup();
                const finalI=i;
                this.markersLayer.addLayer(marker.on('click', function(e) {
                      $('#modalVistaEmpresa').modal('show');
                      //accedemos por jquery a la funcion creada en vuejs
                      $(document).ready(function(){
                          console.log(app.arrayTiendas[finalI].idSucursal);
                          app.latitudEmpresa=app.arrayTiendas[finalI].latitud;
                          app.longitudEmpresa=app.arrayTiendas[finalI].longitud;
                          app.buscarEmpresa(app.arrayTiendas[finalI].idEmpresa, app.arrayTiendas[finalI].idSucursal);
                      });
                    }));
              }
              // --luego de cargar que centree la ubicacion del ususario
              this.map.panTo(new L.LatLng(this.latitude, this.longitude));
              this.vueToast(respuesta.body['message'], 'success');
            }else{
              this.vueToast(respuesta.body['message'], 'error');
            }
          }, function(respuesta) {
            console.log(respuesta.body);
          });
        },
        getIconCategoria(categoria){
           switch(categoria.split("_")[0]) {
             case "Restaurante y cafeterias":
                return L.icon({
                    iconUrl: '<?php echo base_url() ?>public/marker/restaurante.png',
                    iconSize: [45, 54],
                    iconAnchor: [30, 30]
                });
               break;
             case "Tiendas y abarrotes":
               return L.icon({
                    iconUrl: '<?php echo base_url() ?>public/marker/tienda.png',
                    iconSize: [45, 54],
                    iconAnchor: [30, 30]
                });
               break;
              case "Frutas y verduras":
               return L.icon({
                    iconUrl: '<?php echo base_url() ?>public/marker/verduras.png',
                    iconSize: [45, 54],
                    iconAnchor: [30, 30]
                });
               break;
              case "Naturistas y chiflerias":
               return L.icon({
                    iconUrl: '<?php echo base_url() ?>public/marker/verduras.png',
                    iconSize: [45, 54],
                    iconAnchor: [30, 30]
                });
               break;
             default:
                return L.icon({
                    iconUrl: '<?php echo base_url() ?>public/marker/tienda.png',
                    iconSize: [45, 54],
                    iconAnchor: [30, 30]
                });
               // code block
          }
        },
        iniciaMapa(filtro){
          //carga el mapa
          // this.map = L.map('container_maps').setView([51.505, -0.09], 18);
          this.map.locate({setView: true, maxZoom: 17});
          L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
              attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          }).addTo(this.map);
          //crea un capa para los markes
          this.markersLayer.addTo(this.map);
          //limpiar mapa si no hay errores del servidor
          this.markersLayer.clearLayers();
          //carga un icono para el marker
          let iconMarker = L.icon({
              iconUrl: '<?php echo base_url() ?>public/marker/navigation.png',
              iconSize: [60, 60],
              iconAnchor: [30, 60]
          })
          //pide permiso de ubicacion al navegador
          navigator.geolocation.getCurrentPosition(
            (pos) => {
              const { coords } = pos;
              const { latitude, longitude } = coords;
              //crea un marker con las coordenadas
              // L.marker([latitude, longitude], { icon: iconMarker })
              // .addTo(this.map)
              // .openPopup();
              this.latitude=latitude;
              this.longitude=longitude;
              // this.latitude=-16.4763753;
              // this.longitude=-68.27041659999999;
              // var marker= L.marker([latitude, longitude], { icon: iconMarker }).openPopup();
              var marker= L.marker([this.latitude, this.longitude], { icon: iconMarker }).openPopup();
              this.markersLayer.addLayer(marker);
              //guarda las coordenadas del ususario


              console.log("posicion ususario: "+this.latitude+", "+this.longitude);
              //carga las tiendas cercanas
              this.cargaTiendas(filtro);
              // setTimeout(() => {
              // }, 5000)
            },
            (error) => {
              console.log(error)
            },
            {
              enableHighAccuracy: true,
              timeout: 5000,
              maximumAge: 0
          });
        },

        getCoordenadasMetros: function(lat1,lon1,lat2,lon2){
          rad = function(x) {return x*Math.PI/180;}
          var R = 6378.137; //Radio de la tierra en km
          var dLat = rad( lat2 - lat1 );
          var dLong = rad( lon2 - lon1 );
          var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
          var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
          var d = R * c;
          return (d/0.0010000).toFixed(2); //Retorna tres decimales en metros
        },
        vueToast: function(mensaje, tipo){
            //info success error show
            let toast = this.$toasted.show(mensaje, {
                theme: "toasted-primary",
                position: "bottom-center",
                duration : 2000,
                fullWidth: false,
                type: tipo
            });
        }
      },
      created: function() {
        // this.iniciaMapa();
      }
    });
  </script>
</body>
</html>