<style type="text/css">
    #header{
      background-image: url("<?php echo base_url() ?>public/images/background-quibo-developer.jpg");
      background-position: center center;
    }
    #header .menu-item{
      color: #fff;
      font-size: 17px;
      text-align: center;
      padding-top: 15px;
      padding-top: 10px;
      padding-bottom: 10px;
    }
    #header .menu-item:hover{
      background-color: #fff2;
      border-radius: 12px 12px 0 0;
    }
    #header .active{
      background: #fff4;
      border-radius: 12px 12px 0 0;
    }
    #header .no-active{
      background: #fff0;
    }

    #header .input-style{
      background: #fff;
      width: 100%;
      border-radius: 20px;
      border: none;
      background-color: #ffffffba;
      outline: none !important;
      padding: 5px 10px;
    }

    .menuUser{
      bottom: 15px;
      right: 15px;
      padding: 10px;
      border-radius: 10px;
      font-weight: bold;
      color: rgb(242 170 10);
      border-width: 1px;
      border-style: solid;
      border-color: rgb(242 170 10);
    }
  </style>

</head>
<body>
  <section  id="app">
    <!-- //menu -->
    <div class="container-fluid" id="header">
      <div class="row">
        <div class="col-2 col-sm-2 col-md-1 col-lg-1 col-xl-1" style="padding-top: 5px; text-align: right;">
          <img src="<?php echo base_url() ?>public/images/icono-quibo-logo-bolivia.png" width="50">
        </div>
        <div class="col-10 col-sm-10 col-md-3 col-lg-3 col-xl-3"  style="margin-top: 15px;">
          <input class="input-style" type="text" name="txtBuscador" placeholder="¿Que estas buscando?">
        </div>
        <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
          <!-- vacio -->
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" style="padding: 0px;">
          <div class="container-fluid">
            <div class="row" style="margin-top: 15px;">
              <!-- //mostrar solo en dispositivo movil: d-block d-sm-block d-md-none-->
             <!--  <div class="col-6 menu-item d-block d-sm-block d-md-none">
                Cate
                <div class="no-active"></div>
              </div> -->
             <a class="col-6 menu-item no-active" href="<?php echo base_url() ?>Inicio">
                <i class="fas fa-house-damage"></i>Tiendas cercanas
              </a>
              <a class="col-6 menu-item active" href="<?php echo base_url() ?>Perfil">
                <i class="fas fa-users-cog"></i> Mi perfil
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- //final de menu -->
    <div class="container-fluid container_maps" id="container_maps">
      <!-- ::::::::::::::::::   LOGIN -->
      <div class="row" v-if="ventana=='Login'" style="padding-top: 20px">
        <div class="col-1 col-sm-1 col-md-2 col-lg-3 col-xl-3"></div>
        <div class="col-10 col-sm-10 col-md-8 col-lg-6 col-xl-6 card">
          <div style="text-align: center;">
            <div class="card-body">
              <h3 class="card-title">Iniciar session</h3>
              <p class="card-text">Ingresa a tu cuenta de manera facil</p>
              <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">Celular</span>
                </div>
                <input type="number" class="form-control" v-model="numeroUsuario" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
              </div>
              <button class="btn btn-primary" style="background: rgb(242 170 10); border-color: transparent;" v-on:click="iniciarSession()">Ingresar</button><br>
              <p class="card-text">¿No tienes una cuenta aun?</p>
              <p class="card-text" style="text-decoration: underline" v-on:click="muestraRegistro()">Registrarme gratis</p>
            </div>
          </div>
        </div>
        <div class="col-1 col-sm-1 col-md-2 col-lg-3 col-xl-3"></div>
      </div>

      <!-- ::::::::::::::::::   REGISTRA -->
      <div class="row" v-if="ventana=='Registro'" style="padding-top: 20px">
        <div class="col-1 col-sm-1 col-md-2 col-lg-3 col-xl-3"></div>
        <div class="col-10 col-sm-10 col-md-8 col-lg-6 col-xl-6 card">
          <div style="text-align: center;">
            <div class="card-body">
              <h3 class="card-title">Registrar</h3>
              <p class="card-text">Ingresa a tu cuenta de manera facil</p>
              <form method="POST" enctype="multipart/form-data" ref="formRegistro">
                <div class="input-group input-group-sm mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Foto</span>
                  </div>
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" name="foto" id="inputGroupFile01">
                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                  </div>
                </div>
                <div class="input-group input-group-sm mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Celular</span>
                  </div>
                  <input type="number" class="form-control" name="numero" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div class="input-group input-group-sm mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Nombre</span>
                  </div>
                  <input type="text" class="form-control" name="nombres" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div class="input-group input-group-sm mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Apellidos</span>
                  </div>
                  <input type="text" class="form-control" name="apellidos" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                </div>
                <div class="input-group input-group-sm mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">C.I.</span>
                  </div>
                  <input type="number" class="form-control" name="ci" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                </div>
              </form>
              <button class="btn btn-primary" style="background: rgb(242 170 10); border-color: transparent;" v-on:click="registrarCuenta()">Registrar</button><br><br>
            </div>
          </div>
        </div>
        <div class="col-1 col-sm-1 col-md-2 col-lg-3 col-xl-3"></div>
      </div>

      <!-- ::::::::::::::::::   PERFIL -->
      <div class="row" v-if="ventana=='Perfil'" style="padding-top: 20px">
        <div class="col-1 col-sm-1 col-md-2 col-lg-3 col-xl-3"></div>
        <div class="col-10 col-sm-10 col-md-8 col-lg-6 col-xl-6 card">
          <div style="text-align: center;">
            <div class="card-body">
              <h3 class="card-title">Perfil</h3>
              <img width="100" height="100" v-bind:src="BASE_URL+'public/folder/img_cliente_lite/'+ localStorage.foto"><br>

              <div >
                <strong>Nombre: </strong>{{localStorage.nombres}}<br>
                <strong>Apellido: </strong>{{localStorage.apellidos}}<br>
                <strong>C.I: </strong>{{localStorage.ci}}<br>
              </div>

              <div class="row" style="margin-top: 10px">
                <div style="display: flex; width: 100%" class="menuUser">
                  <i class="fas fa-clipboard-list" style="margin-top: 3px;"></i>
                  <div style="padding-left: 10px;">Mis pedidos</div>
                </div>
              </div>

              <div class="row" style="margin-top: 5px">
                <div style="display: flex; width: 100%" class="menuUser">
                  <i class="fas fa-user-circle" style="margin-top: 3px;"></i>
                  <div style="padding-left: 10px;">Mis perfil</div>
                </div>
              </div>

              <div class="row" style="margin-top: 5px">
                <div class="menuUser" style="display: flex; width: 100%; background: rgb(242 170 10); color: #fff" v-on:click="cerraSession()">
                  <i class="fas fa-sign-out-alt" style="margin-top: 3px;"></i>
                  <div style="padding-left: 10px;">Salir de mi cuenta</div>
                </div>
              </div>
              <br><br>
            </div>
          </div>
        </div>
        <div class="col-1 col-sm-1 col-md-2 col-lg-3 col-xl-3"></div>
      </div>


    </div>

    <!-- MODAL DE ALERTA DE USUSARIO NO LOGUEADO -->
    <div class="modal fade" id="modalSinUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="background:#f5da8a">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" style="text-align: center;">
            <h5>Debes iniciar sesion para realizar este pedido</h5>
            <div>
              Para poder realizar un pedido primero debes iniciar session o crear un cuenta  .<br><br>.No te preocupes que es muy facil.<br><br>
            </div>
            <a style="
              bottom: 15px;
              right: 15px;
              padding: 10px;
              border-radius: 20px;
              background: rgb(242 170 10);
              color: #fff;">
               Iniciar sesion o registrate
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script type="text/javascript">
    function initSizeScreen() {
      //redimenciona la ventana de mapa deacuerdo al alto del navegador y del header
      var altoWin=$(window).height();
      var anchoWin=$(window).width();
      var objHeader = document.getElementById('header');
      var objMapa = document.getElementById('container_maps');
      var altoMapa=parseInt(parseInt(altoWin)-objHeader.clientHeight);
      objMapa.style.height = altoMapa+'px';
      // console.log("altoWin: "+altoWin+", altoHeader:"+objHeader.clientHeight+"  altomMapa:"+altoMapa);
    }
    //inicia la primera configuracion de patanlla tanto para el header como para el mapa
    initSizeScreen();
    //se llama a la funcion que detecta el cambio de la pantalla
    $(window).resize(function(){
      // cada vez que se cambie la pantalla se llama a la funcion que redimensiona el mapa y header
      initSizeScreen();
    });


    //iniciarmos el local STORAGE
    if(typeof(Storage)!='undefined'){
      //el navegador es compatible
      console.log("storage init");
    }else{
      //el navegador no es compatible
      console.log("storage fail");
    }

    //iniciarmos nuestro aplicacion con VUE-JS
    const app = new Vue({
      el: '#app',
      data: {
        numeroUsuario:'',
        ventana:'Login',
      },
      methods: {
        muestraRegistro(){
          this.ventana="Registro";
        },
        registrarCuenta: function(){
          //recupera las tiendas segun la coord y filtro
          this.$http.post(
            BASE_URL+'Backend/ApiCliente/setCliente',
            new FormData(this.$refs.formRegistro),
          ).then(function(respuesta) {
            if (respuesta.body['status']) {
              //recupera las tiendas en un array
              var dataJson=respuesta.body['data'];
              localStorage.idCliente=dataJson.idCliente;
              localStorage.nombres=dataJson.nombres;
              localStorage.apellidos=dataJson.apellidos;
              localStorage.numero=dataJson.numero;
              localStorage.ci=dataJson.ci;
              localStorage.foto=dataJson.foto;
              localStorage.estado=dataJson.estado;
              this.ventana="Perfil";
              this.vueToast(respuesta.body['message'], 'success');
            }else{
              this.vueToast(respuesta.body['message'], 'error');
            }
          }, function(respuesta) {
            console.log(respuesta.body);
          });
        },
        iniciarSession: function(){
          //recupera las tiendas segun la coord y filtro
          var formData = new FormData();
          formData.append("numero", this.numeroUsuario);
          this.$http.post(
            BASE_URL+'Backend/ApiCliente/getLoginCliente',
            formData,
            // new FormData(this.$refs.formAddArticulo),
          ).then(function(respuesta) {
            if (respuesta.body['status']) {
              //recupera las tiendas en un array
              var dataJson=respuesta.body['data'];
              localStorage.idCliente=dataJson.idCliente;
              localStorage.nombres=dataJson.nombres;
              localStorage.apellidos=dataJson.apellidos;
              localStorage.numero=dataJson.numero;
              localStorage.ci=dataJson.ci;
              localStorage.foto=dataJson.foto;
              localStorage.estado=dataJson.estado;
              this.ventana="Perfil";
              this.vueToast(respuesta.body['message'], 'success');
            }else{
              this.vueToast(respuesta.body['message'], 'error');
            }
          }, function(respuesta) {
            console.log(respuesta.body);
          });
        },
        cerraSession(){
          this.ventana="Login";
          localStorage.removeItem('idCliente');
        },


        vueToast: function(mensaje, tipo){
            //info success error show
            let toast = this.$toasted.show(mensaje, {
                theme: "toasted-primary",
                position: "bottom-center",
                duration : 2000,
                fullWidth: false,
                type: tipo
            });
        }
      },
      created: function() {
        //verifica que exista un ususario
        if(localStorage.idCliente==null){
          //si no hay ningu ususario muestra login
          this.ventana="Login";
        }else{
          this.ventana="Perfil";
        }
      }
    });
  </script>
</body>
</html>