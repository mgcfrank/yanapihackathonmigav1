<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- Optimizacion y configuracion reposive-->
        <?php setcookie('key', 'value', time() + (7 * 24 * 3600), "/; SameSite=None; Secure");
$GLOBALS['VERSION'] = 0.1;?>
        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <meta content="<?php echo base_url() ?>" name="generator"/>
        <meta content="width=device-width, initial-scale=1, minimum-scale=1" name="viewport"/>
        <meta content="es_ES" property="og:locale"/>
        <link href="<?php echo base_url() ?>public/images/icono-quibo-logo-bolivia.png" rel="shortcut icon" type="image/x-icon"/>
        <!-- Datos para configuracion SEO -->
        <title>
            <?php echo $titulo ?>
        </title>
        <meta content="<?php echo $descripcion; ?>" name="description"/>
        <meta content="<?php echo $titulo; ?>" name="og:title" property="og:title"/>
        <meta content="article" property="og:type"/>
        <meta content="<?php echo $titulo; ?>" property="og:title"/>
        <meta content="<?php echo $descripcion; ?>" property="og:description"/>
        <meta content="<?php echo base_url() ?>public/images/<?php echo $foto; ?>" property="og:image"/>
        <meta content="<?php echo base_url() ?>" property="og:url"/>
        <meta content="<?php echo $descripcion; ?>" property="og:site_name"/>
        <meta content="<?php echo $descripcion; ?>" name="keywords">

        <!-- ::::::::::::::::PWA -->
        <meta name="theme-color" content="#FD6610">
        <meta name="MobileOptimized" content="width">
        <meta name="HandheldFriendly" content="true">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>public/pwa/img/ProgramadorFitness.png">
        <link rel="apple-touch-icon" href="<?php echo base_url() ?>public/pwa/img/ProgramadorFitness.png"><!-- //SCREEN -->
        <link rel="apple-touch-startup-image" href="<?php echo base_url() ?>public/pwa/img/ProgramadorFitness.png"><!-- //ICONO DE LA APP -->
        <link rel="manifest" href="<?php echo base_url() ?>public/pwa/manifest.json">
        <script src="<?php echo base_url() ?>public/pwa/script.js?n=<?php echo ($GLOBALS['VERSION']) ?>"></script>
        <!-- ::::::::::::::::PWA -->

        <!-- Librerias Bootstrap jquery -->
        <script src="<?php echo base_url() ?>public/js/popper.min.js?n=<?php echo ($GLOBALS['VERSION']) ?>"></script>
        <link crossorigin="anonymous" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" rel="stylesheet">
        <link href="<?php echo base_url() ?>public/css/bootstrap.css?n=<?php echo ($GLOBALS['VERSION']) ?>" rel="stylesheet" type="text/css">
        <script src="<?php echo base_url() ?>public/js/jquery-1.9.1.min.js?n=<?php echo ($GLOBALS['VERSION']) ?>"></script>
        <script src="<?php echo base_url() ?>public/js/bootstrap.min.js?n=<?php echo ($GLOBALS['VERSION']) ?>"></script>
        <script src="<?php echo base_url() ?>public/js/bootstrap.bundle.min.js?n=<?php echo ($GLOBALS['VERSION']) ?>" type="text/javascript"></script>
                                                    <!-- PREVIEW IMAGE -->
        <script src="<?php echo base_url() ?>public/js/viewer.js?n=<?php echo ($GLOBALS['VERSION']) ?>">
        </script>
        <link href="<?php echo base_url() ?>public/css/viewer.css?n=<?php echo ($GLOBALS['VERSION']) ?>" rel="stylesheet" type="text/css">
                                                        <!-- VUE JS -->
        <script src="<?php echo base_url() ?>public/js/vue.min.js?n=<?php echo ($GLOBALS['VERSION']) ?>"></script>
        <script src="<?php echo base_url() ?>public/js/vue-resource.min.js?n=<?php echo ($GLOBALS['VERSION']) ?>"></script>
        <script src="<?php echo base_url() ?>public/js/vue-toasted.min.js?n=<?php echo ($GLOBALS['VERSION']) ?>"></script>

 <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin=""></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA--2vIgqXcdDPNQ0xXdmUQWb_DDy9zJCk"></script> -->

<!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.6.1/lodash.js">
    </script>
        <script src="<?php echo base_url() ?>public/js/vue-google-maps.js"></script> -->

        <script type="text/javascript">
            // VueGoogleMap.load({
            //     key: 'AIzaSyA--2vIgqXcdDPNQ0xXdmUQWb_DDy9zJCk'
            // });
            // Vue.component('google-map', VueGoogleMap.Map);
            Vue.use(Toasted);
            var BASE_URL='https://yanapi.quiboo.org/';
        </script>

<!--         <script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA--2vIgqXcdDPNQ0xXdmUQWb_DDy9zJCk&callback=initMap"> -->
<!-- </script> -->




<!--         <script src='https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.js'></script>
        <link href='https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.css' rel='stylesheet' />
 -->
    </head>
</html>